﻿namespace Web.Models.Categorias
{
    public class ActualizarDirectorioViewModel
    {
        public int IdCategoria { get; set; }

        public int Nivel { get; set; }
    }
}
