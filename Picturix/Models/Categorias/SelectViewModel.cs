﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Categorias
{
    public class SelectViewModel
    {
        public int IdCategoria { get; set; }

        public string Nombre { get; set; }
    }
}
