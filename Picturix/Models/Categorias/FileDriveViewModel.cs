﻿using System.Collections.Generic;

namespace Web.Models.Categorias
{
    public class FileDriveViewModel
    {
        public int id { get; set; }

        public string idgoogledrive { get; set; }

        public string nombre { get; set; }

        public int padre { get; set; }

        public List<FileDriveViewModel> children { get; set; }

        public bool estatus { get; set; }

        public int? nivel { get; set; }

        public bool isfile { get; set; }

        public string urlgoogledrive { get; set; }

        public double precio { get; set; }

        public FileDriveViewModel() { children = new List<FileDriveViewModel>(); }
    }
}
