﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Categorias
{
    public class ActualizarViewModel
    {

        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "El nombre no debe tener más de 50 caracteres")]
        public string Nombre { get; set; }

        public int Padre { get; set; }

        public bool Isfile { get; set; }

        public int Idempresa { get; set; }

        public double Precio { get; set; }
    }
}
