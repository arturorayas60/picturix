﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Categorias
{
    public class CategoriaViewModel
    {
        public int id { get; set; }
        
        public string nombre { get; set; }

        public int padre { get; set; }

        public List<CategoriaViewModel> children { get; set; }

        public bool estatus { get; set; }

        public int? nivel { get; set; }

    }
}
