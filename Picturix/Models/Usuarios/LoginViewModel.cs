﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Usuarios
{
    public class LoginViewModel
    {
        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
