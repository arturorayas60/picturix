﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Usuarios
{
    public class MostrarViewModel
    {
        public int IDUsuario { get; set; }
        public int IDPerfil { get; set; }
        public string Usuario { get; set; }
    }
}
