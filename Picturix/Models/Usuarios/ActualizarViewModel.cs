﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Usuarios
{
    public class ActualizarViewModel
    {
        public int IDUsuario { get; set; }
        public int IDPerfil { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
