﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Usuarios
{
    public class CrearUsuarioEmpresaViewModel
    {
        public int IDPerfil { get; set; }

        public int IDEmpresa { get; set; }

        [Required]
        [StringLength(90, ErrorMessage = "El nombre de usuario no debe tener más de 90 caracteres")]
        public string Nombre { get; set; }

        public string Password { get; set; }
    }
}
