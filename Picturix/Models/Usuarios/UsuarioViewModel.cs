﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Usuarios
{
    public class UsuarioViewModel
    {
		public int IDUsuario { get; set; }

		public int IDPerfil { get; set; }

		public string Nombre { get; set; }

		public string NombrePerfil { get; set; }

		public byte[] Password_hash { get; set; }
	
		public bool Estatus { get; set; }

	}
}
