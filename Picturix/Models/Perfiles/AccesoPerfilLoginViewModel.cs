﻿namespace Web.Models.Perfiles
{
    public class AccesoPerfilLoginViewModel
    {
        public int IDPerfil { get; set; }

        public int IDModulo { get; set; }

        public string Modulo { get; set; }

        public bool Estatus { get; set; }
    }
}
