﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Modulos;

namespace Web.Models.Perfiles
{
    public class CrearViewModel
    {
        public int IDEmpresa { get; set; }
        public string Nombre { get; set; }
        public int  Nivel { get; set; }
        public List<ModuloViewModel> modulos { get; set; }
    }
}
