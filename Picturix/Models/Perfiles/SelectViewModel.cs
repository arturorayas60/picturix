﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Perfiles
{
    public class SelectViewModel
    {
        public int IDPerfil { get; set; }
        public string Nombre { get; set; }
    }
}
