﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Perfiles
{
    public class PerfilViewModel
    {
        public int IDPerfil { get; set; }
        public string Nombre { get; set; }
        public int Nivel { get; set; }
    }
}
