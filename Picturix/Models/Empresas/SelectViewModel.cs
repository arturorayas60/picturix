﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Empresas
{
    public class SelectViewModel
    {
        public int IDEmpresa { get; set; }

        public string NombreComercial { get; set; }

    }
}
