﻿using Entities.Direcciones;
using Entities.Empresas;
using Entities.Telefonos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Telefonos;

namespace Web.Models.Empresas
{
    public class EmpresaViewModel
    {

        public int IDEmpresa { get; set; }

        public string RFC { get; set; }

        public string NombreComercial { get; set; }

        public string RazonSocial { get; set; }

        public bool? Estatus { get; set; }

    }
}
