﻿using Entities.Direcciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Direcciones;
using Web.Models.Telefonos;

namespace Web.Models.Empresas
{
    public class ActualizarViewModel
    {
        public int IDEmpresa { get; set; }

        [Required]
        public string RFC { get; set; }
        [Required]
        public string NombreComercial { get; set; }
        [Required]
        public string RazonSocial { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public List<DireccionViewModel> direcciones { get; set; }

        public List<TelefonoViewModel> telefonos { get; set; }
    }
}
