﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Web.Models.Direcciones;
using Web.Models.Telefonos;

namespace Entities.Empresas
{
    public class CrearViewModel
    {

        [Required]
        public string RFC { get; set; }
        [Required]
        public string NombreComercial { get; set; }
        [Required]
        public string RazonSocial { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public bool? Estatus { get; set; }

        public List<DireccionViewModel> direcciones { get; set; }

        public List<TelefonoViewModel> telefonos { get; set; }

        public CrearViewModel() { Estatus = true; direcciones = new List<DireccionViewModel>(); telefonos = new List<TelefonoViewModel>(); }
    }
}
