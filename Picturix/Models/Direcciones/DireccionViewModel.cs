﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Direcciones
{
    public class DireccionViewModel
    {

        public int IDDireccion { get; set; }

        public string Identificador { get; set; }

        public string Pais { get; set; }

        public int IDEstado { get; set; }

        public string Estado { get; set; }

        public int IDMunicipio { get; set; }

        public string Municipio { get; set; }

        public int IDLocalidad { get; set; }

        public string Localidad { get; set; }

        public int IDColonia { get; set; }

        public string Colonia { get; set; }

        public string Calle { get; set; }

        public string NoInterior { get; set; }

        public string NoExterior { get; set; }

        public string EntreCalles { get; set; }

        public string Referencias { get; set; }

        public string Origen { get; set; }
    }
}
