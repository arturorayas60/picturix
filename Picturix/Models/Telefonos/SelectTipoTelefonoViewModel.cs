﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Telefonos
{
    public class SelectTipoTelefonoViewModel
    {
        public int IDTipoTelefono { get; set; }
        public string Nombre { get; set; }
    }
}
