﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Telefonos
{
    public class TelefonoViewModel
    {
        public int IDTelefono { get; set; }
        public string Lada { get; set; }
        public string NoTelefono { get; set; }
        public int IDTipoTelefono { get; set; }
        public string TipoTelefono { get; set; }
        public string Origen { get; set; }

    }
}
