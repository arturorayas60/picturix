﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Sucursal
{
    public class SucursalViewModel
    {
        public int IDSucursal { get; set; }

        public string Nombre { get; set; }
        public string Identificador { get; set; }

        public string HoraApertura { get; set; }

        public string HoraCierre { get; set; }

        public bool Estatus { get; set; }

        public string Tipo { get; set; }


    }
}
