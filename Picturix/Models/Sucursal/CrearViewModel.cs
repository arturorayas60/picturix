﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Telefonos;

namespace Web.Models.Sucursal
{
    public class CrearViewModel
    {
        [Required]
        public int IDEmpresa { get; set; }
        [Required]
        public int IDDireccion { get; set; }

        [Required]
        [StringLength(90, ErrorMessage = "El nombre no debe tener más de 90 caracteres")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "El identificador no debe tener más de 30 caracteres")]
        public string Identificador { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "La hora de apertura no debe tener más de 5 caracteres")]
        public string HoraApertura { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "El hora de cierre no debe tener más de 5 caracteres")]
        public string HoraCierre { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "El tipo no debe tener más de 20 caracteres")]
        public string Tipo { get; set; }

        [Required]
        [StringLength(90, ErrorMessage = "La zona horaria no debe tener más de 90 caracteres")]
        public string ZonaHoraria { get; set; }

        public List<TelefonoViewModel> telefonos { get; set; }

        public List<LicenciaViewModel> licencias { get; set; }


    }
}
