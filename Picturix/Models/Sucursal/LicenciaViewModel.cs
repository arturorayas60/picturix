﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Sucursal
{
    public class LicenciaViewModel
    {
        public int IDLicencia { get; set; } 
        public int IDSucursal { get; set; }
        public Guid Codigo { get; set; }
        public bool Estatus { get; set; }
        public bool Ocupacion { get; set; }
        public string Origen { get; set; }

    }
}
