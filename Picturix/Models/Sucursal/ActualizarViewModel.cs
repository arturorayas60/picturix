﻿using Entities.Direcciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Telefonos;

namespace Web.Models.Sucursal
{
    public class ActualizarViewModel
    {

        public int IDSucursal { get; set; }

        public int IDEmpresa { get; set; }

        public int IDDireccion { get; set; }

        public string Nombre { get; set; }

        public string Identificador { get; set; }

        public string HoraApertura { get; set; }

        public string HoraCierre { get; set; }

        public string Tipo { get; set; }

        public string ZonaHoraria { get; set; }

        public Direccion direccion { get; set; }

        public List<LicenciaViewModel> licencias { get; set; }

        public List<TelefonoViewModel> telefonos { get; set; }
    }
}
