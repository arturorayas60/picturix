﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Email
{
    public class ActivationCompanyRequest
    {

        public string RFC { get; set; }

        public string NombreComercial { get; set; }

        public string RazonSocial { get; set; }
    }
}
