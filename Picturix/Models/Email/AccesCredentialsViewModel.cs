﻿namespace Web.Models.Email
{
    public class AccesCredentialsViewModel
    {
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public string Email { get; set; }
    }
}
