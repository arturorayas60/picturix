﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Modulos
{
    public class ModuloViewModel
    {
        public int IDModulo { get; set; }

        public string Nombre { get; set; }

        public bool Estatus { get; set; }
    }
}
