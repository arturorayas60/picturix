﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models.Encargados
{
    public class EncargadoViewModel
    {
        public int IDEncargado { get; set; }
        public string NombreCompleto { get; set; }
        public string Usuario { get; set; }
        public bool Estatus { get; set; }

    }
}
