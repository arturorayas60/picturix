﻿using Entities.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Direcciones;
using Web.Models.Empresas;
using Web.Models.Sucursal;
using Web.Models.Telefonos;

namespace Web.Models.Encargados
{
    public class CrearViewModel
    {
        public Persona persona { get; set; }
        public Usuarios.CrearViewModel usuario { get; set; }
        public int IDEmpresa { get; set; }
        public List<DireccionViewModel> direcciones { get; set; }
        public List<TelefonoViewModel> telefonos { get; set; }
        public List<SucursalViewModel> sucursales { get; set; }

    }
}
