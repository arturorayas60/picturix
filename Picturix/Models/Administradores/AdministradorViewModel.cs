﻿using Entities.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Direcciones;
using Web.Models.Empresas;
using Web.Models.Telefonos;

namespace Web.Models.Administradores
{
    public class AdministradorViewModel
    {
        public int IDAdministrador { get; set; }
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public string Apellidos { get; set; }
        public string Empresa { get; set; }
        public bool Estatus { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public int IDEmpresa { get; set; }

    }
}
