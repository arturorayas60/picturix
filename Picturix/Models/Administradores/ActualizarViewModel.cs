﻿using Entities.Empresas;
using Entities.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Direcciones;
using Web.Models.Empresas;
using Web.Models.Modulos;
using Web.Models.Telefonos;

namespace Web.Models.Administradores
{
    public class ActualizarViewModel
    {
        public int IDAdministrador { get; set; }
        public Persona persona { get; set; }
        public Usuarios.CrearViewModel usuario { get; set; }
        public EmpresaViewModel empresa { get; set; }

        public List<DireccionViewModel> direcciones { get; set; }

        public List<TelefonoViewModel> telefonos { get; set; }

        public List<ModuloViewModel> modulos { get; set; }
    }
}
