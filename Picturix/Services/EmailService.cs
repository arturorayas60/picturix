﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;
using Web.Models.Email;

namespace Web.Services
{
    public class EmailService
    {
        public string ServerSMTP { get; set; }

        public string UserSMTP { get; set; }

        public string Password { get; set; }

        public EmailService()
        {
            ServerSMTP = "mail.compuandsoft.com";
            UserSMTP = "website@compuandsoft.com";
            Password = "+K7;v0{?SgVX";
        }

        public async Task SendActivationCompanyRequest(ActivationCompanyRequest activationCompanyRequest)
        {

            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("soporte", UserSMTP);
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress("arturo", "arturorayas60@gmail.com");
            message.To.Add(to);

            message.Subject = "Solicitud de activación de empresa";

            BodyBuilder bodyBuilder = new BodyBuilder();
            string templateText =
                @"<table align=""center"" border = ""0"" cellpadding = ""0"" cellspacing = ""0"" width = ""600"" style = ""border:1px solid #cccccc;border-collapse:collapse"">
                   <tbody>
                       <tr>
                           <td align=""center"" bgcolor=""#ffffff"" style=""padding:40px 0 30px 0;color:#153643;font-size:12pt;font-weight:bold;font-family:Arial,sans-serif"">
                               <a href=""https://www.compuandsoft.com"">
                                 <img src=""https://www.compuandsoft.com/img/logo.png"" alt=""logo"" width=""300"" height=""103"">                        
                               </a>                        
                           </td>                        
                       </tr>                        
                       <tr>                      
                           <td bgcolor=""#ffffff"" style=""padding:40px 30px 40px 30px"">                           
                              <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">                                  
                                    <tbody>                                  
                                           <tr>                                  
                                               <td style=""color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  Se acaba de registrar en picturix la siguiente empresa:                                                   
                                               </td>
							               </tr>
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> RFC:</b> {0}
								               </td>
							               </tr>							               
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> NombreComercial:</b> {1}
								               </td>
							               </tr>
							               <tr>
								               <td style=""padding:20px 0 30px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> RazonSocial:</b> {2}
								               </td>
							               </tr>
							               <tr>
								               <td style=""padding:20px 0 30px 0;color:#ff1800;font-family:Arial,sans-serif;font-size:12pt;line-height:20px;text-align:center"">
                                                    Para que esta empresa pueda crear licencias es necesario activarla en Picturix (http://picturix.gearhostpreview.com/)
								               </td>
							               </tr>
						            </tbody>
					          </table>
				         </td>
			        </tr>
			        <tr>
				         <td bgcolor=""#03357d"" style=""padding:30px 30px 30px 30px"">
                             <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                    <tbody>
                                           <tr>
                                                <td style=""color:#ffffff;font-family:Arial,sans-serif;font-size:12pt"" width=""75%"">
                                                    Compuandsoft © 2020 - Picturix.<br>
								                </td>
							               </tr>
						            </tbody>
					         </table>
				         </td>
			        </tr>
		         </tbody>
	           </table>";

            bodyBuilder.HtmlBody = string.Format(templateText, activationCompanyRequest.RFC, activationCompanyRequest.NombreComercial, activationCompanyRequest.RazonSocial);

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect(ServerSMTP, 465, true);
            client.Authenticate(UserSMTP, Password);
            await client.SendAsync(message);
            await client.DisconnectAsync(true);
            client.Dispose();

        }

        public async Task SendAccessCredentials(AccesCredentialsViewModel credentials)
        {

            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("soporte", UserSMTP);
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress(credentials.Usuario, credentials.Email);
            message.To.Add(to);

            message.Subject = "Credenciales de acceso";

            BodyBuilder bodyBuilder = new BodyBuilder();
            string templateText =
                @"<table align=""center"" border = ""0"" cellpadding = ""0"" cellspacing = ""0"" width = ""600"" style = ""border:1px solid #cccccc;border-collapse:collapse"">
                   <tbody>
                       <tr>
                           <td align=""center"" bgcolor=""#ffffff"" style=""padding:40px 0 30px 0;color:#153643;font-size:12pt;font-weight:bold;font-family:Arial,sans-serif"">
                               <a href=""https://www.compuandsoft.com"">
                                 <img src=""https://www.compuandsoft.com/img/logo.png"" alt=""logo"" width=""300"" height=""103"">                        
                               </a>                        
                           </td>                        
                       </tr>                        
                       <tr>                      
                           <td bgcolor=""#ffffff"" style=""padding:40px 30px 40px 30px"">                           
                              <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">                                  
                                    <tbody>                                  
                                           <tr>                                  
                                               <td style=""color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  La credenciales de picturix ha sido modificadas, estas son tus nuevas credenciales de acceso:                                                  
                                               </td>
							               </tr>
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> Usuario:</b> {0}
								               </td>
							               </tr>							               
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> Contraseña:</b> {1}
								               </td>
							               </tr>							             
							               <tr>
								               <td style=""padding:20px 0 30px 0;font-family:Arial,sans-serif;font-size:12pt;line-height:20px;text-align:center"">
                                                    Recuerda que puedes usar estas credenciales para acceder a la aplicación de escritorio o en la web de Picturix (http://picturix.gearhostpreview.com/)
								               </td>
							               </tr>
						            </tbody>
					          </table>
				         </td>
			        </tr>
			        <tr>
				         <td bgcolor=""#03357d"" style=""padding:30px 30px 30px 30px"">
                             <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                    <tbody>
                                           <tr>
                                                <td style=""color:#ffffff;font-family:Arial,sans-serif;font-size:12pt"" width=""75%"">
                                                    Compuandsoft © 2020 - Picturix.<br>
								                </td>
							               </tr>
						            </tbody>
					         </table>
				         </td>
			        </tr>
		         </tbody>
	           </table>";

            bodyBuilder.HtmlBody = string.Format(templateText, credentials.Usuario, credentials.Contraseña);

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect(ServerSMTP, 465, true);
            client.Authenticate(UserSMTP, Password);
            await client.SendAsync(message);
            await client.DisconnectAsync(true);
            client.Dispose();
        }

        public async Task SendInitialAccessCredentials(AccesCredentialsViewModel credentials)
        {

            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("soporte", UserSMTP);
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress(credentials.Usuario, credentials.Email);
            message.To.Add(to);

            message.Subject = "Accesos para Picturix";

            BodyBuilder bodyBuilder = new BodyBuilder();
            string templateText =
                @"<table align=""center"" border = ""0"" cellpadding = ""0"" cellspacing = ""0"" width = ""600"" style = ""border:1px solid #cccccc;border-collapse:collapse"">
                   <tbody>
                       <tr>
                           <td align=""center"" bgcolor=""#ffffff"" style=""padding:40px 0 30px 0;color:#153643;font-size:12pt;font-weight:bold;font-family:Arial,sans-serif"">
                               <a href=""https://www.compuandsoft.com"">
                                 <img src=""https://www.compuandsoft.com/img/logo.png"" alt=""logo"" width=""300"" height=""103"">                        
                               </a>                        
                           </td>                        
                       </tr>                        
                       <tr>                      
                           <td bgcolor=""#ffffff"" style=""padding:40px 30px 40px 30px"">                           
                              <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">                                  
                                    <tbody>                                  
                                           <tr>                                  
                                               <td style=""color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  Bienvenido(a) a Picturix, tus credenciales para iniciar sesión son las siguientes:                                                  
                                               </td>
							               </tr>
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> Usuario:</b> {0}
								               </td>
							               </tr>							               
							               <tr>
								               <td style=""padding:20px 0 0px 0; color:#153643;font-family:Arial,sans-serif;font-size:12pt"">
                                                  <b> Contraseña:</b> {1}
								               </td>
							               </tr>							             
							               <tr>
								               <td style=""padding:20px 0 30px 0;font-family:Arial,sans-serif;font-size:12pt;line-height:20px;text-align:center"">
                                                    Recuerda que puedes usar estas credenciales para acceder a la aplicación de escritorio o en la web de Picturix (http://picturix.gearhostpreview.com/)
								               </td>
							               </tr>
						            </tbody>
					          </table>
				         </td>
			        </tr>
			        <tr>
				         <td bgcolor=""#03357d"" style=""padding:30px 30px 30px 30px"">
                             <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"">
                                    <tbody>
                                           <tr>
                                                <td style=""color:#ffffff;font-family:Arial,sans-serif;font-size:12pt"" width=""75%"">
                                                    Compuandsoft © 2020 - Picturix.<br>
								                </td>
							               </tr>
						            </tbody>
					         </table>
				         </td>
			        </tr>
		         </tbody>
	           </table>";

            bodyBuilder.HtmlBody = string.Format(templateText, credentials.Usuario, credentials.Contraseña);

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect(ServerSMTP, 465, true);
            client.Authenticate(UserSMTP, Password);
            await client.SendAsync(message);
            await client.DisconnectAsync(true);
            client.Dispose();
        }

    }
}
