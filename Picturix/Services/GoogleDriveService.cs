﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Web.Services.Service
{
    public class GoogleDriveService
    {
        private DriveService _DriveService = null;
        private const string _ApplicationName = "picturix";
        public string IdFile = string.Empty;
        public string IdParent = string.Empty;

        public GoogleDriveService() { }

        public void LoadFiles()
        {
            if (this._DriveService == null)
                this._DriveService = this.getService();

            // Define parameters of request.
            FilesResource.ListRequest listRequest = this._DriveService.Files.List();
            listRequest.Fields = "files(id, name,mimeType,description,starred,trashed,parents,fullFileExtension,fileExtension)";

            // List files
            var files = listRequest.Execute().Files.Where(_ => _.MimeType != "application/vnd.google-apps.folder");
            var folders = listRequest.Execute().Files.Where(_ => _.MimeType == "application/vnd.google-apps.folder");
        }

        private DriveService getService()
        {
            var crendetial = GoogleAuthService.Auth();

            return new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = crendetial,
                ApplicationName = _ApplicationName
            });
        }

        public bool CreateFolder(string categoria, string Idpadre)
        {
            try
            {
                if (this._DriveService == null)
                    this._DriveService = this.getService();

                Google.Apis.Drive.v3.Data.File folder = new Google.Apis.Drive.v3.Data.File();
                folder.Name = categoria;
                folder.MimeType = "application/vnd.google-apps.folder";
                folder.Parents = new List<string>();

                if (string.IsNullOrEmpty(Idpadre))
                {
                    FilesResource.ListRequest listRequest = this._DriveService.Files.List();
                    listRequest.Fields = "files(id, name,mimeType,description,starred,trashed,parents,fullFileExtension,fileExtension)";

                    // List folders
                    var raiz = listRequest.Execute().Files.Where(_ => _.MimeType == "application/vnd.google-apps.folder").ToList().Find(_ => _.Name == "monografia");

                    folder.Parents.Add(raiz.Id);
                }
                else
                    folder.Parents.Add(Idpadre);

                Google.Apis.Drive.v3.Data.File folderCreate = this._DriveService.Files.Create(folder).Execute();
                this.IdFile = folderCreate.Id;

                return true;
            }
            catch (System.Exception e)
            {
                string message = e.Message;
                return false;
            }
        }

        public void MoveFolder(string idfile, string idfolderdestino)
        {
            var searchFiles = this._DriveService.Files.List();
            searchFiles.Q = "id = '" + idfile + "'";
            searchFiles.Fields = "files(*)";

            var fileToMove = searchFiles.Execute().Files[0];

            var updateRequest = this._DriveService.Files.Update(new Google.Apis.Drive.v3.Data.File(), idfile);
            updateRequest.AddParents = idfolderdestino;
            updateRequest.RemoveParents = fileToMove.Parents[0];
            var movedFile = updateRequest.Execute();
        }

        public void UpdateFolder(bool rename, string idfolder, string namefolder, bool move, string idfolderdestino)
        {
            try
            {
                if (this._DriveService == null)
                    this._DriveService = this.getService();

                if (rename)
                {
                    var folder = new Google.Apis.Drive.v3.Data.File();
                    folder.Name = namefolder;
                    var updateRequest = this._DriveService.Files.Update(folder, idfolder);
                    var fileRenamed = updateRequest.Execute();
                }

                if (move)
                    MoveFiles(idfolder, idfolderdestino);
            }
            catch (System.Exception e)
            {
                string message = e.Message;
            }
        }

        public void MoveFiles(String fileId, String folderId)
        {
            if (this._DriveService == null)
                this._DriveService = this.getService();

            Google.Apis.Drive.v3.DriveService service = this._DriveService;

            // Retrieve the existing parents to remove
            Google.Apis.Drive.v3.FilesResource.GetRequest getRequest = service.Files.Get(fileId);
            getRequest.Fields = "parents";
            Google.Apis.Drive.v3.Data.File file = getRequest.Execute();
            string previousParents = String.Join(",", file.Parents);

            // Move the file to the new folder
            Google.Apis.Drive.v3.FilesResource.UpdateRequest updateRequest = service.Files.Update(new Google.Apis.Drive.v3.Data.File(), fileId);
            updateRequest.Fields = "id, parents";
            updateRequest.AddParents = folderId;
            updateRequest.RemoveParents = previousParents;

            file = updateRequest.Execute();
            this.IdFile = folderId;

        }

        public async Task<IEnumerable<Google.Apis.Drive.v3.Data.File>> GetFiles(string idfolder, List<string> parents)
        {
            if (this._DriveService == null)
                this._DriveService = this.getService();

            // Define parameters of request.
            FilesResource.ListRequest listRequest = this._DriveService.Files.List();
            listRequest.Fields = "files(id, name,mimeType,description,starred,trashed,parents,fullFileExtension,fileExtension)";

            // get files from google drive
            var photos = await Task.FromResult(listRequest.Execute().Files.
               Where(_ =>
               _.FileExtension == "jpeg" ||
               _.FileExtension == "jpg" ||
                _.FileExtension == "png" ||
               _.FileExtension == "gif" ||
               _.FileExtension == "tiff").
               Where(_ => _.Trashed == false));
            //Where(_ => _.MimeType != "application/vnd.google-apps.folder"));
           
            List<Google.Apis.Drive.v3.Data.File> filesfiltred = new List<Google.Apis.Drive.v3.Data.File>();

            foreach (var file in photos)
                foreach (var item in parents)
                    if (item == idfolder)
                        filesfiltred.Add(file);

            return filesfiltred;
        }

    }

    public static class GoogleAuthService
    {
        private const string _ClientId = "206345366634-ldcsfftkl17k0qnlk1oamu564pp2uq6o.apps.googleusercontent.com";
        private const string _ClientSecret = "fnxI7Zci4o047uW4nMISOMNi";

        public static UserCredential Auth()
        {
            UserCredential credential;
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = _ClientId,
                    ClientSecret = _ClientSecret
                },
                 new[] { DriveService.Scope.Drive },
                "user",
                CancellationToken.None
            ).Result;
            return credential;
        }
    }
}