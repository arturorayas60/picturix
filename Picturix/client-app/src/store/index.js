import Vue from "vue";
import Vuex from "vuex";
import decode from "jwt-decode";
import router from "../router";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    usuario: null,
    empresa: null,
    loading: false,
    loadingCircular: false,
    accesos: []
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    setAccesos(state, accesos) {
      state.accesos = accesos;
    },
    setUsuario(state, usuario) {
      state.usuario = usuario;
    },
    setEmpresa(state, empresa) {
      state.empresa = empresa;
    },
    setLoading(state, visible) {
      state.loading = visible;
    },
    setLoadingCircular(state, visible) {
      state.loadingCircular = visible;
    }
  },
  actions: {
    guardarToken({ commit }, token) {
      commit("setToken", token);
      commit("setUsuario", decode(token));
      localStorage.setItem("token", token);
    },
    autoLogin({ commit }) {
      let token = localStorage.getItem("token");
      let empresa = localStorage.getItem("empresa");
      let accesos = localStorage.getItem("accesos");

      if (token) {
        commit("setToken", token);
        commit("setUsuario", decode(token));
      }
      if (empresa) {
        commit("setEmpresa", JSON.parse(empresa));
      }
      if (accesos) {
        commit("setAccesos", JSON.parse(accesos));
      }

      /*if (router.history.current.path != '/home') {
        router.push({ name: "home" });
      }*/
    },
    salir({ commit }) {
      commit("setToken", null);
      commit("setUsuario", null);
      commit("setEmpresa", null);
      commit("setAccesos", null);

      localStorage.removeItem("token");
      localStorage.removeItem("accesos");
      localStorage.removeItem("empresa");

      if (router.history.current.path !== "/login") {
        router.push({ name: "login" });
      }
    },
    cambiarEmpresa({ commit }, empresa) {
      commit("setEmpresa", empresa);
      localStorage.setItem("empresa", JSON.stringify(empresa));
    },
    guardarAccesos({ commit }, accesos) {
      commit("setAccesos", accesos);
      localStorage.setItem("accesos", JSON.stringify(accesos));
    },
    mostrarOcultarProgressbar({ commit }, estatus) {
      commit("setLoading", estatus);
    },
    mostrarOcultarProgressbarcircular({ commit }, estatus) {
      commit("setLoadingCircular", estatus);
    }
  }
});
