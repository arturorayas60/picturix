import Vue from "vue";
import Router from "vue-router";
import Home from "../components/HelloWorld.vue";
import Login from "../components/Login.vue";
import Empresa from "../components/Empresa.vue";
import CambiarEmpresa from "../components/CambiarEmpresa.vue";
import Categoria from "../components/Categoria.vue";
import Perfil from "../components/Perfil.vue";
import Administrador from "../components/Administrador.vue";
import Sucursal from "../components/Sucursal.vue";
import Encargado from "../components/Encargado.vue";
import Usuario from "../components/Usuario.vue";
import Registro from "../components/Private/Registro/Register.vue"
import store from "../store";

Vue.use(Router);

var router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: true,
      },
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        libre: true,
      },
    },
    {
      path: "/registro",
      name: "registro",
      component: Registro,
      meta: {
        libre: true,
      },
    },
    {
      path: "/categorias",
      name: "categorias",
      component: Categoria,
      meta: {
        superadmin: true,
        administrador: false,
        encargado: false,
      },
    },
    {
      path: "/empresas",
      name: "empresas",
      component: Empresa,
      meta: {
        superadmin: true,
        administrador: false,
        encargado: false,
      },
    },
    {
      path: "/perfiles",
      name: "perfiles",
      component: Perfil,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: false,
      },
    },
    {
      path: "/administradores",
      name: "administradores",
      component: Administrador,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: false,
      },
    },
    {
      path: "/sucursales",
      name: "sucursales",
      component: Sucursal,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: false,
      },
    },
    {
      path: "/encargados",
      name: "encargados",
      component: Encargado,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: false,
      },
    },
    {
      path: "/usuarios",
      name: "usuarios",
      component: Usuario,
      meta: {
        superadmin: true,
        administrador: true,
        encargado: false,
      },
    },
    {
      path: "/cambiarempresa",
      name: "cambiarempresa",
      component: CambiarEmpresa,
      meta: {
        superadmin: true,
        administrador: false,
        encargado: false,
      },
    },
  ],
});

router.beforeEach(async (to, from, next) => {
  if (store.state.usuario == null) {
    await store.dispatch("autoLogin");
  }

  if (to.matched.some((record) => record.meta.libre)) {
    if (to.name == "registro" && store.state.token) {
      next({ name: "home" });
    } else {
      next();
    }
  } else {
    if (store.state.usuario) {

      if (to.name == "usuarios") {
        if (
          store.state.accesos.find((a) => a.modulo == "Usuarios" && a.estatus)
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "encargados") {
        if (
          store.state.accesos.find((a) => a.modulo == "Encargados" && a.estatus)
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "sucursales") {
        if (
          store.state.accesos.find((a) => a.modulo == "Sucursales" && a.estatus)
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "administradores") {
        if (
          store.state.accesos.find(
            (a) => a.modulo == "Administradores" && a.estatus
          )
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "perfiles") {
        if (
          store.state.accesos.find((a) => a.modulo == "Perfiles" && a.estatus)
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "empresas") {
        if (
          store.state.accesos.find((a) => a.modulo == "Empresas" && a.estatus)
        ) {
          console.log("Empresas");
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "categorias") {
        if (
          store.state.accesos.find((a) => a.modulo == "Categorias" && a.estatus)
        ) {
          next();
        } else {
          next({ name: "home" });
        }
      } 

      if (to.name == "home" || to.name == "cambiarempresa") {
        next();
      }

      if (to.name == "login") {
        next({ name: "home" });
      }

    } else {
      if (from == null) {
        next({ name: "login" });
      } else {
        if (from.name != "login") {
          next({ name: "login" });
        }
      }
    }
  }
});

export default router;
