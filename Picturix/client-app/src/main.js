import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'

Vue.config.productionTip = false
//local base URL
//axios.defaults.baseURL="https://localhost:44313/"
//axios.defaults.baseURL = "http://localhost:60335/"
axios.defaults.baseURL = ""
//production base URL
//axios.defaults.baseURL='https://picturixservice.xyz/'

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
