﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Entities.Modulos;
using Web.Models.Modulos;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class ModulosController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public ModulosController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Modulos/Listar
        [HttpGet("[action]/{nivel}")]
        public async Task<IEnumerable<ModuloViewModel>> Listar([FromRoute] int nivel)
        {
            var modulo = await _context.Modulos.Where(_=>_.Nivel==nivel).ToListAsync();

            return modulo.Select(e => new ModuloViewModel
            {
                IDModulo = e.IDModulo,
                Nombre=e.Nombre,
                Estatus = true
            }) ;
        }

        private bool ModuloExists(int id)
        {
            return _context.Modulos.Any(e => e.IDModulo == id);
        }
    }
}
