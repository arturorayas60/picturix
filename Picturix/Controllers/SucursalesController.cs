﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Entities.Sucursales;
using Web.Models.Sucursal;
using Entities.Telefonos;
using Web.Models.Telefonos;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class SucursalesController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public SucursalesController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Sucursales/Listar
        [HttpGet("[action]/{IDEmpresa}")]
        public async Task<IEnumerable<SucursalViewModel>> Listar(int IDEmpresa)
        {
            var sucursal = await _context.Sucursales.Where(s => s.IDEmpresa == IDEmpresa).ToListAsync();

            return sucursal.Select(s => new SucursalViewModel
            {
                Estatus = s.Estatus,
                Nombre = s.Nombre,
                HoraApertura = s.HoraApertura,
                HoraCierre = s.HoraCierre,
                Identificador = s.Identificador,
                IDSucursal = s.IDSucursal,
                Tipo = s.Tipo

            });
        }

        // GET: api/Sucursales/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var sucursal = _context.Sucursales.Include(_ => _.direccion).SingleOrDefault(x => x.IDSucursal == id);

            if (sucursal == null)
            {
                return NotFound();
            }

            var telefonos = await _context.Telefonos.Include(t => t.tipotelefono).ToListAsync();
            var telefonosuscursal = await _context.TelefonosSucursal.ToListAsync();

            var licencias = await _context.Licencias.Where(_ => _.IDSucursal == id).ToArrayAsync();

            return Ok(new ActualizarViewModel
            {
                IDSucursal = sucursal.IDSucursal,
                Identificador = sucursal.Identificador,
                Tipo = sucursal.Tipo,
                HoraApertura = sucursal.HoraApertura,
                HoraCierre = sucursal.HoraCierre,
                IDDireccion = sucursal.direccion.IDDireccion,
                IDEmpresa = sucursal.IDEmpresa,
                Nombre = sucursal.Nombre,
                direccion = sucursal.direccion,
                ZonaHoraria = sucursal.ZonaHoraria,
                licencias = licencias.Select(e => new LicenciaViewModel
                {
                    IDLicencia = e.IDLicencia,
                    Codigo = e.Codigo,
                    Estatus = e.Estatus,
                    Ocupacion = e.Ocupacion,
                    Origen = "BD"
                }).ToList(),
                telefonos = telefonos.Select(e => new TelefonoViewModel
                {
                    IDTelefono = e.IDTelefono,
                    IDTipoTelefono = e.IDTipoTelefono,
                    Lada = e.Lada,
                    NoTelefono = e.NoTelefono,
                    TipoTelefono = e.tipotelefono.Nombre
                }).Where(t2 => telefonosuscursal.Where(d => d.IDSucursal == sucursal.IDSucursal).Any(d1 => t2.IDTelefono == d1.IDTelefono)).ToList(),
            });
        }

        // POST: api/Sucursales/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Sucursal sucursal = new Sucursal
            {
                Identificador = model.Identificador,
                HoraApertura = model.HoraApertura,
                HoraCierre = model.HoraCierre,
                IDEmpresa = model.IDEmpresa,
                IDDireccion = model.IDDireccion,
                Nombre = model.Nombre,
                Tipo = model.Tipo,
                ZonaHoraria = model.ZonaHoraria,
                Estatus = true
            };

            _context.Sucursales.Add(sucursal);

            await _context.SaveChangesAsync();

            var idsucursal = sucursal.IDSucursal;

            foreach (var item in model.telefonos)
            {
                Telefono telefono = new Telefono()
                {
                    IDTipoTelefono = item.IDTipoTelefono,
                    Lada = item.Lada,
                    NoTelefono = item.NoTelefono
                };

                _context.Telefonos.Add(telefono);
                await _context.SaveChangesAsync();

                var idtelefono = telefono.IDTelefono;
                TelefonoSucursal telefonosucursal = new TelefonoSucursal()
                {
                    IDTelefono = idtelefono,
                    IDSucursal = idsucursal
                };

                _context.TelefonosSucursal.Add(telefonosucursal);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.licencias)
            {
                Licencia licencia = new Licencia()
                {
                    IDSucursal = idsucursal,
                    Estatus = item.Estatus,
                    Ocupacion = false,
                    Codigo = item.Codigo
                };

                _context.Licencias.Add(licencia);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }

        // PUT: api/Sucursales/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IDSucursal <= 0)
            {
                return BadRequest();
            }

            var sucursal = await _context.Sucursales.FirstOrDefaultAsync(c => c.IDSucursal == model.IDSucursal);

            if (sucursal == null)
            {
                return NotFound();
            }

            sucursal.Identificador = model.Identificador;
            sucursal.Nombre = model.Nombre;
            sucursal.Tipo = model.Tipo;
            sucursal.HoraApertura = model.HoraApertura;
            sucursal.HoraCierre = model.HoraCierre;
            sucursal.IDDireccion = model.IDDireccion;
            sucursal.Tipo = model.Tipo;
            sucursal.ZonaHoraria = model.ZonaHoraria;

            await _context.SaveChangesAsync();

            foreach (var tel in model.telefonos)
            {
                //Eliminar 
                if (tel.Origen == "eliminar")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(t => t.IDTelefono == tel.IDTelefono);

                    if (telefono == null) { return NotFound(); }

                    var telsucursal = await _context.TelefonosSucursal.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    _context.TelefonosSucursal.Remove(telsucursal);

                    await _context.SaveChangesAsync();

                    _context.Telefonos.Remove(telefono);
                    await _context.SaveChangesAsync();

                }

                //Agregar
                if (tel.Origen == "Nuevo")
                {
                    Telefono telefono = new Telefono()
                    {
                        IDTipoTelefono = tel.IDTipoTelefono,
                        Lada = tel.Lada,
                        NoTelefono = tel.NoTelefono
                    };

                    _context.Telefonos.Add(telefono);
                    await _context.SaveChangesAsync();

                    var idtelefono = telefono.IDTelefono;
                    TelefonoSucursal telefonoSucursal = new TelefonoSucursal()
                    {
                        IDTelefono = idtelefono,
                        IDSucursal = model.IDSucursal
                    };

                    _context.TelefonosSucursal.Add(telefonoSucursal);

                    await _context.SaveChangesAsync();
                }

                //Editar
                if (tel.Origen == "Editado")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    telefono.Lada = tel.Lada;
                    telefono.NoTelefono = tel.NoTelefono;
                    telefono.IDTipoTelefono = tel.IDTipoTelefono;

                    await _context.SaveChangesAsync();
                }

            }

            foreach (var lic in model.licencias)
            {
                //Eliminar 
                if (lic.Origen == "eliminar")
                {
                    var licencia = await _context.Licencias.FirstOrDefaultAsync(t => t.IDLicencia == lic.IDLicencia);

                    if (licencia == null) { return NotFound(); }

                    _context.Licencias.Remove(licencia);
                    await _context.SaveChangesAsync();
                }

                //Agregar
                if (lic.Origen == "Nuevo")
                {
                    Licencia licencia = new Licencia()
                    {
                        IDSucursal = model.IDSucursal,
                        Ocupacion = lic.Ocupacion,
                        Estatus = lic.Estatus,
                        Codigo = lic.Codigo
                    };

                    _context.Licencias.Add(licencia);
                    await _context.SaveChangesAsync();
                }

                //Editar
                if (lic.Origen == "Editado")
                {
                    var licencia = await _context.Licencias.FirstOrDefaultAsync(i => i.IDLicencia == lic.IDLicencia);

                    licencia.Ocupacion = lic.Ocupacion;
                    licencia.Estatus = lic.Estatus;
                    licencia.Codigo = lic.Codigo;

                    await _context.SaveChangesAsync();
                }

            }

            return Ok();
        }

        //PUT: api/Sucursales/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var sucursal = await _context.Sucursales.FirstOrDefaultAsync(e => e.IDSucursal == id);

            if (sucursal == null)
            {
                return NotFound();
            }

            sucursal.Estatus = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Sucursales/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var sucursal = await _context.Sucursales.FirstOrDefaultAsync(e => e.IDSucursal == id);

            if (sucursal == null)
            {
                return NotFound();
            }

            sucursal.Estatus = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        // GET: api/Sucursales/ListarZonasHorarias
        [HttpGet("[action]")]
        public IEnumerable<ZonaHorariaViewModel> ListarZonasHorarias([FromQuery] int IDEmpresa)
        {
            var zonashorarias = TimeZoneInfo.GetSystemTimeZones().ToList();

            return zonashorarias.Select(z => new ZonaHorariaViewModel
            {
                Id = z.Id,
                Name = z.DisplayName
            });
        }

        private bool SucursalExists(int id)
        {
            return _context.Sucursales.Any(e => e.IDSucursal == id);
        }
    }
}
