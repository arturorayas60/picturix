﻿using Data;
using Entities.Empresas;
using Entities.Perfiles;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Modulos;
using Web.Models.Perfiles;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class PerfilesController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public PerfilesController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Perfiles/ListarPerfilesEmpresa/3/0
        [HttpGet("[action]/{nivel}/{idempresa}")]
        public async Task<IEnumerable<PerfilViewModel>> ListarPerfilesEmpresa([FromRoute] int nivel, int idempresa)
        {

            if (idempresa == 0)
            {
                var perfil = await _context.Perfiles.Where(_ => _.Nivel == 3 && _.IDPerfil != 3).ToListAsync();

                return perfil.Select(p => new PerfilViewModel
                {
                    IDPerfil = p.IDPerfil,
                    Nivel = p.Nivel,
                    Nombre = p.Nombre
                });
            }
            else
            {
                var empresa = await _context.Empresas.Where(_ => _.IDEmpresa == idempresa).ToListAsync();
                var perfil = await _context.Perfiles.Where(_ => _.Nivel == nivel).ToListAsync();

                if (empresa != null)
                {
                    var perfileempresa = await _context.PerfilesEmpresas.Where(_ => _.IDEmpresa == idempresa).ToListAsync();

                    return perfil.Select(p => new PerfilViewModel
                    {
                        IDPerfil = p.IDPerfil,
                        Nivel = p.Nivel,
                        Nombre = p.Nombre
                    }).Where(t2 => perfileempresa.Where(d => d.IDEmpresa == idempresa).Any(d1 => t2.IDPerfil == d1.IDPerfil)).ToList();
                }
            }

            var aux = new List<PerfilViewModel>();
            return aux.Select(p => new PerfilViewModel
            {
                IDPerfil = p.IDPerfil,
                Nivel = p.Nivel,
                Nombre = p.Nombre
            });
        }

        // GET: api/Perfiles/ListarAccesosPerfil/3
        [HttpGet("[action]/{idperfil}")]
        public async Task<IEnumerable<AccesoPerfilLoginViewModel>> ListarAccesosPerfil([FromRoute] int idperfil)
        {

            var perfilesacceso = await _context.AccesosPerfiles.Include(_ => _.modulo).Where(_ => _.IDPerfil == idperfil).ToListAsync();

            return perfilesacceso.Select(p => new AccesoPerfilLoginViewModel()
            {
                IDModulo = p.IDModulo,
                IDPerfil = p.IDPerfil,
                Modulo = p.modulo.Nombre,
                Estatus = p.Estatus
            });
        }

        // GET: api/Perfiles/Select
        [HttpGet("[action]/{nivel}")]
        public async Task<IEnumerable<SelectViewModel>> Select([FromRoute] int nivel)
        {
            try
            {

                var perfil = await _context.Perfiles.Where(_ => _.IDPerfil == nivel).ToListAsync();

                return perfil.Select(p => new SelectViewModel
                {
                    IDPerfil = p.IDPerfil,
                    Nombre = p.Nombre
                });

            }
            catch (Exception)
            {
                var aux = new List<SelectViewModel>();
                return aux.Select(p => new SelectViewModel
                {
                    IDPerfil = p.IDPerfil,
                    Nombre = p.Nombre
                });
            }
        }

        // GET: api/Perfiles/SelectAvanzado
        [HttpGet("[action]/{nivel}/{idempresa}")]
        public async Task<IEnumerable<SelectViewModel>> SelectAvanzado([FromRoute] int nivel, int idempresa)
        {
            try
            {
                if (idempresa == 0 && nivel == 3)
                {
                    var perfil = await _context.Perfiles.Where(_ => _.Nivel == 3 && _.IDPerfil > 3).ToListAsync();

                    return perfil.Select(p => new SelectViewModel
                    {
                        IDPerfil = p.IDPerfil,
                        Nombre = p.Nombre
                    });
                }
                else
                {
                    var perfil = await _context.Perfiles.Where(_ => _.Nivel == nivel && _.IDPerfil > 3).ToListAsync();
                    var perfileempresa = await _context.PerfilesEmpresas.Include(_ => _.perfil).Where(_ => _.IDEmpresa == idempresa).ToListAsync();

                    foreach (var item in perfileempresa)
                    {
                        if (perfil.Find(_ => _.IDPerfil == item.perfil.IDPerfil) != null)
                            perfil.Add(item.perfil);
                    }

                    return perfil.Select(p => new SelectViewModel
                    {
                        IDPerfil = p.IDPerfil,
                        Nombre = p.Nombre
                    });
                }
            }
            catch (Exception)
            {
                var aux = new List<SelectViewModel>();
                return aux.Select(p => new SelectViewModel
                {
                    IDPerfil = p.IDPerfil,
                    Nombre = p.Nombre
                });
            }
        }

        // POST: api/Perfiles/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] Web.Models.Perfiles.CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                Perfil perfil = new Perfil()
                {
                    Nivel = model.Nivel,
                    Nombre = model.Nombre,
                };

                _context.Perfiles.Add(perfil);
                await _context.SaveChangesAsync();

                if (model.IDEmpresa > 0)
                {
                    PerfilEmpresa perfilempresa = new PerfilEmpresa()
                    {
                        IDPerfil = perfil.IDPerfil,
                        IDEmpresa = model.IDEmpresa
                    };

                    _context.PerfilesEmpresas.Add(perfilempresa);
                    await _context.SaveChangesAsync();
                }

                foreach (var item in model.modulos)
                {
                    AccesoPerfil accesoPerfil = new AccesoPerfil()
                    {
                        IDModulo = item.IDModulo,
                        IDPerfil = perfil.IDPerfil,
                        Estatus = item.Estatus,
                    };

                    _context.AccesosPerfiles.Add(accesoPerfil);
                    await _context.SaveChangesAsync();
                }

            }
            catch (System.Exception)
            {
                return BadRequest();
            }

            return Ok();
        }

        // GET: api/Perfiles/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var perfil = await _context.Perfiles.FindAsync(id);

            if (perfil == null)
            {
                return NotFound();
            }

            var modulosacceso = await _context.AccesosPerfiles.Where(m => m.IDPerfil == perfil.IDPerfil).ToArrayAsync();

            return Ok(new ActualizarViewModel
            {
                IDPerfil = perfil.IDPerfil,
                Nombre = perfil.Nombre,
                Nivel = perfil.Nivel,
                modulos = modulosacceso.Select(e => new ModuloViewModel
                {
                    IDModulo = e.IDModulo,
                    Estatus = e.Estatus
                }).ToList(),
            });
        }

        // PUT: api/Administradores/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (model.IDPerfil <= 0)
                {
                    return BadRequest();
                }

                var perfil = await _context.Perfiles.FirstOrDefaultAsync(a => a.IDPerfil == model.IDPerfil);

                if (perfil == null)
                {
                    return NotFound();
                }

                perfil.Nombre = model.Nombre;
                await _context.SaveChangesAsync();

                foreach (var acce in model.modulos)
                {
                    var accesomodulo = await _context.AccesosPerfiles.FirstOrDefaultAsync(u => u.IDPerfil == model.IDPerfil && u.IDModulo == acce.IDModulo);
                    if (accesomodulo != null)
                    {
                        accesomodulo.Estatus = acce.Estatus;
                        await _context.SaveChangesAsync();
                    }
                }
            }
            catch (Exception)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        private bool PerfilExists(int id)
        {
            return _context.Perfiles.Any(e => e.IDPerfil == id);
        }
    }
}
