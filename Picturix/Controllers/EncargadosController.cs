﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Entities.Encargados;
using Web.Models.Encargados;
using Entities.Usuarios;
using Entities.Personas;
using Entities.Direcciones;
using Entities.Telefonos;
using Web.Models.Sucursal;
using Web.Models.Direcciones;
using Web.Models.Telefonos;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class EncargadosController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public EncargadosController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Encargados/Listar
        [HttpGet("[action]/{IDEmpresa}")]
        public async Task<IEnumerable<EncargadoViewModel>> Listar(int IDEmpresa)
        {
            var encargado = await _context.Encargados.Include(_ => _.persona).Include(u => u.usuario).Where(s => s.IDEmpresa == IDEmpresa).ToListAsync();

            return encargado.Select(s => new EncargadoViewModel
            {
                IDEncargado = s.IDEncargado,
                NombreCompleto = s.persona.Nombre + " " + s.persona.ApellidoPaterno + " " + s.persona.ApellidoMaterno,
                Usuario = s.usuario.Nombre,
                Estatus = s.Estatus
            });
        }

        // GET: api/Encargados/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var encargado = await _context.Encargados.FindAsync(id);

            if (encargado == null)
            {
                return NotFound();
            }

            var direccionesencargado = await _context.DireccionesEncargado.ToListAsync();
            var direcciones = await _context.Direcciones.ToListAsync();

            var telefonos = await _context.Telefonos.Include(t => t.tipotelefono).ToListAsync();
            var telefonosencargado = await _context.TelefonosEncargado.ToListAsync();

            var accesos = await _context.AccesosSucursalEncargado.Include(_ => _.sucursal).Where(_ => _.IDEncargado == id).ToListAsync();

            var persona = await _context.Personas.FindAsync(encargado.IDPersona);

            var usuario = await _context.Usuarios.FindAsync(encargado.IDUsuario);

            return Ok(new Web.Models.Encargados.ActualizarViewModel
            {
                IDEncargado = id,
                sucursales = accesos.Select(e => new SucursalViewModel
                {
                    IDSucursal = e.IDSucursal,
                    Estatus = e.Estatus,
                    Nombre = e.sucursal.Nombre,
                    HoraApertura = e.sucursal.HoraApertura,
                    HoraCierre = e.sucursal.HoraCierre,
                    Identificador = e.sucursal.Identificador,
                    Tipo = e.sucursal.Tipo
                }).ToList(),
                persona = new Persona()
                {
                    IDPersona = persona.IDPersona,
                    ApellidoMaterno = persona.ApellidoMaterno,
                    ApellidoPaterno = persona.ApellidoPaterno,
                    Email = persona.Email,
                    FechaNacimiento = persona.FechaNacimiento,
                    Nombre = persona.Nombre,
                    Sexo = persona.Sexo
                },
                usuario = new Models.Usuarios.CrearViewModel()
                {
                    IDPerfil = usuario.IDPerfil,
                    Nombre = usuario.Nombre,
                },
                direcciones = direcciones.Select(e => new DireccionViewModel
                {
                    IDDireccion = e.IDDireccion,
                    Identificador = e.Identificador,
                    Calle = e.Calle,
                    IDColonia = e.IDColonia,
                    Colonia = e.Colonia,
                    IDEstado = e.IDEstado,
                    IDLocalidad = e.IDLocalidad,
                    IDMunicipio = e.IDMunicipio,
                    Localidad = e.Localidad,
                    EntreCalles = e.EntreCalles,
                    Estado = e.Estado,
                    Municipio = e.Municipio,
                    NoExterior = e.NoExterior,
                    NoInterior = e.NoInterior,
                    Pais = e.Pais,
                    Referencias = e.Referencias

                }).Where(t2 => direccionesencargado.Where(d => d.IDEncargado == encargado.IDEncargado).Any(d1 => t2.IDDireccion == d1.IDDireccion)).ToList(),
                telefonos = telefonos.Select(e => new TelefonoViewModel
                {
                    IDTelefono = e.IDTelefono,
                    IDTipoTelefono = e.IDTipoTelefono,
                    Lada = e.Lada,
                    NoTelefono = e.NoTelefono,
                    TipoTelefono = e.tipotelefono.Nombre
                }).Where(t2 => telefonosencargado.Where(d => d.IDEncargado == encargado.IDEncargado).Any(d1 => t2.IDTelefono == d1.IDTelefono)).ToList(),
            });
        }

        // POST: api/Encargados/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] Web.Models.Encargados.CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CrearPasswordHash(model.usuario.Nombre, out byte[] passwordHash, out byte[] passwordSalt);

            Usuario usuario = new Usuario
            {
                IDPerfil = model.usuario.IDPerfil,
                Nombre = model.usuario.Nombre,
                Password_hash = passwordHash,
                Password_salt = passwordSalt,
                Estatus = true
            };

            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            var idusuario = usuario.IDUsuario;

            Persona persona = new Persona()
            {
                Nombre = model.persona.Nombre,
                ApellidoMaterno = model.persona.ApellidoMaterno,
                ApellidoPaterno = model.persona.ApellidoPaterno,
                Email = model.persona.Email,
                FechaNacimiento = model.persona.FechaNacimiento,
                Sexo = model.persona.Sexo
            };

            _context.Personas.Add(persona);
            await _context.SaveChangesAsync();

            var idpersona = persona.IDPersona;

            Encargado encargado = new Encargado
            {
                IDEmpresa = model.IDEmpresa,
                IDPersona = idpersona,
                IDUsuario = idusuario,
                Estatus = true
            };

            _context.Encargados.Add(encargado);

            await _context.SaveChangesAsync();

            var idencargado = encargado.IDEncargado;

            foreach (var item in model.direcciones)
            {

                Direccion direccion = new Direccion()
                {
                    Identificador = item.Identificador.Trim(),
                    Calle = item.Calle.Trim(),
                    Colonia = item.Colonia.Trim(),
                    EntreCalles = item.EntreCalles.Trim(),
                    IDColonia = item.IDColonia,
                    IDEstado = item.IDEstado,
                    IDLocalidad = item.IDLocalidad,
                    IDMunicipio = item.IDMunicipio,
                    Estado = item.Estado.Trim(),
                    Localidad = item.Localidad.Trim(),
                    Municipio = item.Municipio.Trim(),
                    NoExterior = item.NoExterior.Trim(),
                    NoInterior = item.NoInterior.Trim(),
                    Pais = item.Pais.Trim(),
                    Referencias = item.Referencias.Trim()
                };

                _context.Direcciones.Add(direccion);
                await _context.SaveChangesAsync();

                var iddireccion = direccion.IDDireccion;
                DireccionEncargado direccionEncargado = new DireccionEncargado()
                {
                    IDDireccion = iddireccion,
                    IDEncargado = idencargado
                };

                _context.DireccionesEncargado.Add(direccionEncargado);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.telefonos)
            {
                Telefono telefono = new Telefono()
                {
                    IDTipoTelefono = item.IDTipoTelefono,
                    Lada = item.Lada,
                    NoTelefono = item.NoTelefono
                };

                _context.Telefonos.Add(telefono);
                await _context.SaveChangesAsync();

                var idtelefono = telefono.IDTelefono;
                TelefonoEncargado telefonoencargado = new TelefonoEncargado()
                {
                    IDTelefono = idtelefono,
                    IDEncargado = idencargado
                };

                _context.TelefonosEncargado.Add(telefonoencargado);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.sucursales)
            {
                AccesoSucursalEncargado accesosucursalencargado = new AccesoSucursalEncargado()
                {
                    IDEncargado = idencargado,
                    IDSucursal = item.IDSucursal,
                    Estatus = item.Estatus
                };
                _context.AccesosSucursalEncargado.Add(accesosucursalencargado);
                await _context.SaveChangesAsync();
            }
            return Ok();
        }

        // PUT: api/Administradores/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] Web.Models.Encargados.ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IDEncargado <= 0)
            {
                return BadRequest();
            }

            var encargado = await _context.Encargados.FirstOrDefaultAsync(a => a.IDEncargado == model.IDEncargado);

            if (encargado == null)
            {
                return NotFound();
            }

            var persona = await _context.Personas.FirstOrDefaultAsync(p => p.IDPersona == encargado.IDPersona);

            persona.Email = model.persona.Email;
            persona.ApellidoPaterno = model.persona.ApellidoPaterno;
            persona.ApellidoMaterno = model.persona.ApellidoMaterno;
            persona.FechaNacimiento = model.persona.FechaNacimiento;
            persona.Nombre = model.persona.Nombre;
            persona.Sexo = model.persona.Sexo;
            await _context.SaveChangesAsync();

            if (!string.IsNullOrEmpty(model.usuario.Password))
            {
                var usuario = await _context.Usuarios.FirstOrDefaultAsync(u => u.IDUsuario == encargado.IDUsuario);
                CrearPasswordHash(model.usuario.Password, out byte[] passwordHash, out byte[] passwordSalt);
                usuario.Password_hash = passwordHash;
                usuario.Password_salt = passwordSalt;
            }

            await _context.SaveChangesAsync();

            foreach (var dir in model.direcciones)
            {
                //Eliminar
                if (dir.Origen == "eliminar")
                {
                    var direccion = await _context.Direcciones.FirstOrDefaultAsync(t => t.IDDireccion == dir.IDDireccion);

                    if (direccion == null) { return NotFound(); }

                    var direncargado = await _context.DireccionesEncargado.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                    _context.DireccionesEncargado.Remove(direncargado);

                    await _context.SaveChangesAsync();

                    _context.Direcciones.Remove(direccion);
                    await _context.SaveChangesAsync();
                }

                //Agregar
                if (dir.Origen == "Nuevo")
                {
                    Direccion direccion = new Direccion()
                    {
                        Identificador = dir.Identificador,
                        Calle = dir.Calle,
                        Colonia = dir.Colonia,
                        EntreCalles = dir.EntreCalles,
                        IDColonia = dir.IDColonia,
                        IDEstado = dir.IDEstado,
                        IDLocalidad = dir.IDLocalidad,
                        IDMunicipio = dir.IDMunicipio,
                        Estado = dir.Estado,
                        Localidad = dir.Localidad,
                        Municipio = dir.Municipio,
                        NoExterior = dir.NoExterior,
                        NoInterior = dir.NoInterior,
                        Pais = dir.Pais,
                        Referencias = dir.Referencias
                    };

                    _context.Direcciones.Add(direccion);
                    await _context.SaveChangesAsync();

                    var iddireccion = direccion.IDDireccion;
                    DireccionEncargado direccionEncargado = new DireccionEncargado()
                    {
                        IDDireccion = iddireccion,
                        IDEncargado = encargado.IDEncargado
                    };

                    _context.DireccionesEncargado.Add(direccionEncargado);

                    await _context.SaveChangesAsync();
                }

                //Editar
                if (dir.Origen == "Editado")
                {
                    var direccion = await _context.Direcciones.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                    direccion.IDColonia = dir.IDColonia;
                    direccion.Identificador = dir.Identificador;
                    direccion.IDEstado = dir.IDEstado;
                    direccion.IDLocalidad = dir.IDLocalidad;
                    direccion.IDMunicipio = dir.IDMunicipio;
                    direccion.Localidad = dir.Localidad;
                    direccion.Municipio = dir.Municipio;
                    direccion.NoExterior = dir.NoExterior;
                    direccion.NoInterior = dir.NoInterior;
                    direccion.Pais = dir.Pais;
                    direccion.Referencias = dir.Referencias;
                    direccion.Estado = dir.Estado;
                    dir.EntreCalles = dir.EntreCalles;
                    dir.Colonia = dir.Colonia;
                    dir.Calle = dir.Calle;

                    await _context.SaveChangesAsync();
                }
            }

            foreach (var tel in model.telefonos)
            {
                //Eliminar 
                if (tel.Origen == "eliminar")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(t => t.IDTelefono == tel.IDTelefono);

                    if (telefono == null) { return NotFound(); }

                    var telencargado = await _context.TelefonosEncargado.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    _context.TelefonosEncargado.Remove(telencargado);

                    await _context.SaveChangesAsync();

                    _context.Telefonos.Remove(telefono);
                    await _context.SaveChangesAsync();
                }

                //Agregar
                if (tel.Origen == "Nuevo")
                {
                    Telefono telefono = new Telefono()
                    {
                        IDTipoTelefono = tel.IDTipoTelefono,
                        Lada = tel.Lada,
                        NoTelefono = tel.NoTelefono
                    };

                    _context.Telefonos.Add(telefono);
                    await _context.SaveChangesAsync();

                    var idtelefono = telefono.IDTelefono;
                    TelefonoEncargado telefonoencargado = new TelefonoEncargado()
                    {
                        IDTelefono = idtelefono,
                        IDEncargado = encargado.IDEncargado
                    };

                    _context.TelefonosEncargado.Add(telefonoencargado);

                    await _context.SaveChangesAsync();
                }

                //Editar
                if (tel.Origen == "Editado")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    telefono.Lada = tel.Lada;
                    telefono.NoTelefono = tel.NoTelefono;
                    telefono.IDTipoTelefono = tel.IDTipoTelefono;

                    await _context.SaveChangesAsync();
                }

            }

            foreach (var acce in model.sucursales)
            {
                var accesosucursal = await _context.AccesosSucursalEncargado.FirstOrDefaultAsync(u => u.IDEncargado == encargado.IDEncargado && u.IDSucursal == acce.IDSucursal);
                if (accesosucursal != null)
                {
                    accesosucursal.Estatus = acce.Estatus;
                    await _context.SaveChangesAsync();
                }
            }

            return Ok();
        }


        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        private bool EncargadoExists(int id)
        {
            return _context.Encargados.Any(e => e.IDEncargado == id);
        }
    }
}
