﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Entities.Telefonos;
using Web.Models.Telefonos;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class TelefonosController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public TelefonosController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Telefonos/SelectTipoTelefono
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectTipoTelefonoViewModel>> SelectTipoTelefono()
        {
            var tipostelefono = await _context.TiposTelefonos.ToListAsync();

            return tipostelefono.Select(e => new SelectTipoTelefonoViewModel
            {
                IDTipoTelefono=e.IDTipoTelefono,
                Nombre=e.Nombre
            });
        }

        private bool TelefonoExists(int id)
        {
            return _context.Telefonos.Any(e => e.IDTelefono == id);
        }
    }
}
