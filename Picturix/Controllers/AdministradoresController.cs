﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Web.Models.Administradores;
using Entities.Administradores;
using Entities.Usuarios;
using Entities.Personas;
using Microsoft.Extensions.Configuration;
using Entities.Direcciones;
using Entities.Empresas;
using Entities.Telefonos;
using Web.Models.Direcciones;
using Web.Models.Telefonos;
using Web.Models.Modulos;
using Web.Services;
using Web.Models.Email;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class AdministradoresController : ControllerBase
    {
        private readonly DbContextPicturix _context;
        private readonly IConfiguration _config;

        public AdministradoresController(DbContextPicturix context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/Administradores/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<AdministradorViewModel>> Listar()
        {
            var administrador = await _context.Administradores.Include(p => p.persona).Include(u => u.usuario).Include(e => e.empresa).ToListAsync();

            return administrador.Select(e => new AdministradorViewModel
            {
                IDAdministrador = e.IDAdministrador,
                IDEmpresa = e.IDEmpresa,
                IDPersona = e.IDPersona,
                IDUsuario = e.IDUsuario,
                Apellidos = e.persona.ApellidoMaterno + " " + e.persona.ApellidoPaterno,
                Empresa = e.empresa.NombreComercial,
                Estatus = e.Estatus,
                Nombre = e.persona.Nombre,
                Usuario = e.usuario.Nombre
            });
        }

        // GET: api/Administradores/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var administrador = await _context.Administradores.FindAsync(id);

            if (administrador == null)
            {
                return NotFound();
            }

            var direccionesadministrador = await _context.DireccionesAdministradores.ToListAsync();
            var direcciones = await _context.Direcciones.ToListAsync();

            var telefonos = await _context.Telefonos.Include(t => t.tipotelefono).ToListAsync();
            var telefonosadministrador = await _context.TelefonosAdministradores.ToListAsync();

            var modulosacceso = await _context.AccesosUsuario.Where(m => m.IDUsuario == administrador.IDUsuario).ToArrayAsync();

            var empresa = await _context.Empresas.FindAsync(administrador.IDEmpresa);

            var persona = await _context.Personas.FindAsync(administrador.IDPersona);

            var usuario = await _context.Usuarios.FindAsync(administrador.IDUsuario);

            return Ok(new ActualizarViewModel
            {
                IDAdministrador = administrador.IDAdministrador,
                modulos = modulosacceso.Select(e => new ModuloViewModel
                {
                    IDModulo = e.IDModulo,
                    Estatus = e.Estatus
                }).ToList(),
                empresa = new Models.Empresas.EmpresaViewModel()
                {
                    IDEmpresa = empresa.IDEmpresa,
                    NombreComercial = empresa.NombreComercial,
                    RFC = empresa.RFC,
                    RazonSocial = empresa.RazonSocial
                },
                persona = new Persona()
                {
                    IDPersona = persona.IDPersona,
                    ApellidoMaterno = persona.ApellidoMaterno,
                    ApellidoPaterno = persona.ApellidoPaterno,
                    Email = persona.Email,
                    FechaNacimiento = persona.FechaNacimiento,
                    Nombre = persona.Nombre,
                    Sexo = persona.Sexo
                },
                usuario = new Models.Usuarios.CrearViewModel()
                {
                    IDPerfil = usuario.IDPerfil,
                    Nombre = usuario.Nombre,
                },
                direcciones = direcciones.Select(e => new DireccionViewModel
                {
                    IDDireccion = e.IDDireccion,
                    Identificador = e.Identificador,
                    Calle = e.Calle,
                    IDColonia = e.IDColonia,
                    Colonia = e.Colonia,
                    IDEstado = e.IDEstado,
                    IDLocalidad = e.IDLocalidad,
                    IDMunicipio = e.IDMunicipio,
                    Localidad = e.Localidad,
                    EntreCalles = e.EntreCalles,
                    Estado = e.Estado,
                    Municipio = e.Municipio,
                    NoExterior = e.NoExterior,
                    NoInterior = e.NoInterior,
                    Pais = e.Pais,
                    Referencias = e.Referencias

                }).Where(t2 => direccionesadministrador.Where(d => d.IDAdministrador == administrador.IDAdministrador).Any(d1 => t2.IDDireccion == d1.IDDireccion)).ToList(),
                telefonos = telefonos.Select(e => new TelefonoViewModel
                {
                    IDTelefono = e.IDTelefono,
                    IDTipoTelefono = e.IDTipoTelefono,
                    Lada = e.Lada,
                    NoTelefono = e.NoTelefono,
                    TipoTelefono = e.tipotelefono.Nombre
                }).Where(t2 => telefonosadministrador.Where(d => d.IDAdministrador == administrador.IDAdministrador).Any(d1 => t2.IDTelefono == d1.IDTelefono)).ToList(),
            });
        }

        // POST: api/Administradores/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] Models.Administradores.CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CrearPasswordHash(model.usuario.Password, out byte[] passwordHash, out byte[] passwordSalt);

            Usuario usuario = new Usuario
            {
                IDPerfil = model.usuario.IDPerfil,
                Nombre = model.usuario.Nombre,
                Password_hash = passwordHash,
                Password_salt = passwordSalt,
                Estatus = true
            };

            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            var idusuario = usuario.IDUsuario;

            Persona persona = new Persona()
            {
                Nombre = model.persona.Nombre,
                ApellidoMaterno = model.persona.ApellidoMaterno,
                ApellidoPaterno = model.persona.ApellidoPaterno,
                Email = model.persona.Email,
                FechaNacimiento = model.persona.FechaNacimiento,
                Sexo = model.persona.Sexo
            };

            _context.Personas.Add(persona);
            await _context.SaveChangesAsync();

            var idpersona = persona.IDPersona;

            Administrador administrador = new Administrador
            {
                IDEmpresa = model.IDEmpresa,
                IDPersona = idpersona,
                IDUsuario = idusuario,
                Estatus = true
            };

            _context.Administradores.Add(administrador);

            await _context.SaveChangesAsync();

            var idempresa = model.IDEmpresa;

            foreach (var item in model.direcciones)
            {

                Direccion direccion = new Direccion()
                {
                    Identificador = item.Identificador.Trim(),
                    Calle = item.Calle.Trim(),
                    Colonia = item.Colonia.Trim(),
                    EntreCalles = item.EntreCalles.Trim(),
                    IDColonia = item.IDColonia,
                    IDEstado = item.IDEstado,
                    IDLocalidad = item.IDLocalidad,
                    IDMunicipio = item.IDMunicipio,
                    Estado = item.Estado.Trim(),
                    Localidad = item.Localidad.Trim(),
                    Municipio = item.Municipio.Trim(),
                    NoExterior = item.NoExterior.Trim(),
                    NoInterior = item.NoInterior.Trim(),
                    Pais = item.Pais.Trim(),
                    Referencias = item.Referencias.Trim()
                };

                _context.Direcciones.Add(direccion);
                await _context.SaveChangesAsync();

                var iddireccion = direccion.IDDireccion;
                DireccionEmpresa direccionEmpresa = new DireccionEmpresa()
                {
                    IDDireccion = iddireccion,
                    IDEmpresa = idempresa
                };

                _context.DireccionesEmpresas.Add(direccionEmpresa);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.telefonos)
            {
                Telefono telefono = new Telefono()
                {
                    IDTipoTelefono = item.IDTipoTelefono,
                    Lada = item.Lada,
                    NoTelefono = item.NoTelefono
                };

                _context.Telefonos.Add(telefono);
                await _context.SaveChangesAsync();

                var idtelefono = telefono.IDTelefono;
                TelefonoEmpresa telefonoEmpresa = new TelefonoEmpresa()
                {
                    IDTelefono = idtelefono,
                    IDEmpresa = idempresa
                };

                _context.TelefonosEmpresas.Add(telefonoEmpresa);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.modulos)
            {
                AccesoUsuario accesoUsuario = new AccesoUsuario()
                {
                    IDModulo = item.IDModulo,
                    IDUsuario = idusuario,
                    Estatus = item.Estatus,
                };
                _context.AccesosUsuario.Add(accesoUsuario);
                await _context.SaveChangesAsync();
            }

            EmailService service = new EmailService();

            AccesCredentialsViewModel accesCredentials = new AccesCredentialsViewModel()
            {
                Usuario = usuario.Nombre,
                Contraseña = model.usuario.Password,
                Email = model.persona.Email
            };
            await service.SendInitialAccessCredentials(accesCredentials);

            return Ok();
        }

        // PUT: api/Administradores/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IDAdministrador <= 0)
            {
                return BadRequest();
            }

            var administrador = await _context.Administradores.FirstOrDefaultAsync(a => a.IDAdministrador == model.IDAdministrador);

            if (administrador == null)
            {
                return NotFound();
            }

            administrador.IDEmpresa = model.empresa.IDEmpresa;
            await _context.SaveChangesAsync();

            var persona = await _context.Personas.FirstOrDefaultAsync(p => p.IDPersona == administrador.IDPersona);

            persona.Email = model.persona.Email;
            persona.ApellidoPaterno = model.persona.ApellidoPaterno;
            persona.ApellidoMaterno = model.persona.ApellidoMaterno;
            persona.FechaNacimiento = model.persona.FechaNacimiento;
            persona.Nombre = model.persona.Nombre;
            persona.Sexo = model.persona.Sexo;
            await _context.SaveChangesAsync();

            if (!string.IsNullOrEmpty(model.usuario.Password))
            {
                var usuario = await _context.Usuarios.FirstOrDefaultAsync(u => u.IDUsuario == administrador.IDUsuario);
                CrearPasswordHash(model.usuario.Password, out byte[] passwordHash, out byte[] passwordSalt);
                usuario.Password_hash = passwordHash;
                usuario.Password_salt = passwordSalt;
            }

            await _context.SaveChangesAsync();

            foreach (var dir in model.direcciones)
            {
                //Eliminar
                if (dir.Origen == "eliminar")
                {
                    var direccion = await _context.Direcciones.FirstOrDefaultAsync(t => t.IDDireccion == dir.IDDireccion);

                    if (direccion == null) { return NotFound(); }

                    var diradministrador = await _context.DireccionesAdministradores.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                    _context.DireccionesAdministradores.Remove(diradministrador);

                    await _context.SaveChangesAsync();

                    _context.Direcciones.Remove(direccion);
                    await _context.SaveChangesAsync();
                }

                //Agregar
                if (dir.Origen == "Nuevo")
                {
                    Direccion direccion = new Direccion()
                    {
                        Identificador = dir.Identificador.Trim(),
                        Calle = dir.Calle.Trim(),
                        Colonia = dir.Colonia.Trim(),
                        EntreCalles = dir.EntreCalles.Trim(),
                        IDColonia = dir.IDColonia,
                        IDEstado = dir.IDEstado,
                        IDLocalidad = dir.IDLocalidad,
                        IDMunicipio = dir.IDMunicipio,
                        Estado = dir.Estado.Trim(),
                        Localidad = dir.Localidad.Trim(),
                        Municipio = dir.Municipio.Trim(),
                        NoExterior = dir.NoExterior.Trim(),
                        NoInterior = dir.NoInterior.Trim(),
                        Pais = dir.Pais.Trim(),
                        Referencias = dir.Referencias.Trim()
                    };

                    _context.Direcciones.Add(direccion);
                    await _context.SaveChangesAsync();

                    var iddireccion = direccion.IDDireccion;
                    DireccionAdministrador direccionAdministrador = new DireccionAdministrador()
                    {
                        IDDireccion = iddireccion,
                        IDAdministrador = administrador.IDAdministrador
                    };

                    _context.DireccionesAdministradores.Add(direccionAdministrador);

                    await _context.SaveChangesAsync();
                }

                //Editar
                if (dir.Origen == "Editado")
                {
                    var direccion = await _context.Direcciones.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                    direccion.IDColonia = dir.IDColonia;
                    direccion.Identificador = dir.Identificador;
                    direccion.IDEstado = dir.IDEstado;
                    direccion.IDLocalidad = dir.IDLocalidad;
                    direccion.IDMunicipio = dir.IDMunicipio;
                    direccion.Localidad = dir.Localidad;
                    direccion.Municipio = dir.Municipio;
                    direccion.NoExterior = dir.NoExterior;
                    direccion.NoInterior = dir.NoInterior;
                    direccion.Pais = dir.Pais;
                    direccion.Referencias = dir.Referencias;
                    direccion.Estado = dir.Estado;
                    dir.EntreCalles = dir.EntreCalles;
                    dir.Colonia = dir.Colonia;
                    dir.Calle = dir.Calle;

                    await _context.SaveChangesAsync();
                }
            }

            foreach (var tel in model.telefonos)
            {
                //Eliminar 
                if (tel.Origen == "eliminar")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(t => t.IDTelefono == tel.IDTelefono);

                    if (telefono == null) { return NotFound(); }

                    var teladministrador = await _context.TelefonosAdministradores.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    _context.TelefonosAdministradores.Remove(teladministrador);

                    await _context.SaveChangesAsync();

                    _context.Telefonos.Remove(telefono);
                    await _context.SaveChangesAsync();
                }

                //Agregar
                if (tel.Origen == "Nuevo")
                {
                    Telefono telefono = new Telefono()
                    {
                        IDTipoTelefono = tel.IDTipoTelefono,
                        Lada = tel.Lada,
                        NoTelefono = tel.NoTelefono
                    };

                    _context.Telefonos.Add(telefono);
                    await _context.SaveChangesAsync();

                    var idtelefono = telefono.IDTelefono;
                    TelefonoAdministrador telefonoAdministrador = new TelefonoAdministrador()
                    {
                        IDTelefono = idtelefono,
                        IDAdministrador = administrador.IDAdministrador
                    };

                    _context.TelefonosAdministradores.Add(telefonoAdministrador);

                    await _context.SaveChangesAsync();
                }

                //Editar
                if (tel.Origen == "Editado")
                {
                    var telefono = await _context.Telefonos.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                    telefono.Lada = tel.Lada;
                    telefono.NoTelefono = tel.NoTelefono;
                    telefono.IDTipoTelefono = tel.IDTipoTelefono;

                    await _context.SaveChangesAsync();
                }

            }

            foreach (var acce in model.modulos)
            {
                var accesomodulo = await _context.AccesosUsuario.FirstOrDefaultAsync(u => u.IDUsuario == administrador.IDUsuario && u.IDModulo == acce.IDModulo);
                if (accesomodulo != null)
                {
                    accesomodulo.Estatus = acce.Estatus;
                    await _context.SaveChangesAsync();
                }
            }


            return Ok();
        }

        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool AdministradorExists(int id)
        {
            return _context.Administradores.Any(e => e.IDAdministrador == id);
        }
    }
}
