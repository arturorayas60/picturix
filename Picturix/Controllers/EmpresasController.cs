﻿using Data;
using Entities.Direcciones;
using Entities.Empresas;
using Entities.Telefonos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Direcciones;
using Web.Models.Email;
using Web.Models.Empresas;
using Web.Models.Telefonos;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class EmpresasController : ControllerBase
    {
        private DbContextPicturix _context;

        public EmpresasController(DbContextPicturix context)
        {
            _context = context;
        }

        // GET: api/Empresas/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<EmpresaViewModel>> Listar()
        {
            var empresa = await _context.Empresas.ToListAsync();

            return empresa.Select(e => new EmpresaViewModel
            {
                IDEmpresa = e.IDEmpresa,
                NombreComercial = e.NombreComercial,
                Estatus = e.Estatus,
                RazonSocial = e.RazonSocial,
                RFC = e.RFC
            });
        }

        // GET: api/Empresas/ListarBuscarEmpresa
        [HttpGet("[action]")]
        public async Task<IEnumerable<EmpresaViewModel>> ListarBuscarEmpresa([FromQuery] string rfc = "", string nombrecomercial = "", string razonsocial = "")
        {
            try
            {
                var empresa = await _context.Empresas.ToListAsync();

                if (!string.IsNullOrEmpty(rfc))
                    empresa = empresa.FindAll(_ => _.RFC.Contains(rfc.Trim()));

                if (!string.IsNullOrEmpty(nombrecomercial))
                    empresa = empresa.FindAll(_ => _.NombreComercial.Contains(nombrecomercial.Trim()));

                if (!string.IsNullOrEmpty(razonsocial))
                    empresa = empresa.FindAll(_ => _.RazonSocial.Contains(razonsocial.Trim()));

                return empresa.Select(e => new EmpresaViewModel
                {
                    IDEmpresa = e.IDEmpresa,
                    NombreComercial = e.NombreComercial,
                    Estatus = false,
                    RazonSocial = e.RazonSocial,
                    RFC = e.RFC
                });
            }
            catch (Exception)
            {

                throw;
            }
        }

        // GET: api/Empresas/ListarDireccionesEmpresa/
        [HttpGet("[action]/{idempresa}")]
        public async Task<IEnumerable<DireccionViewModel>> ListarDireccionesEmpresa(int idempresa)
        {
            var direccionesempresa = await _context.DireccionesEmpresas.Where(d => d.IDEmpresa == idempresa).ToListAsync();
            var direcciones = await _context.Direcciones.ToListAsync();

            return direcciones.Select(e => new DireccionViewModel
            {
                IDDireccion = e.IDDireccion,
                Identificador = e.Identificador,
                Calle = e.Calle,
                IDColonia = e.IDColonia,
                Colonia = e.Colonia,
                IDEstado = e.IDEstado,
                IDLocalidad = e.IDLocalidad,
                IDMunicipio = e.IDMunicipio,
                Localidad = e.Localidad,
                EntreCalles = e.EntreCalles,
                Estado = e.Estado,
                Municipio = e.Municipio,
                NoExterior = e.NoExterior,
                NoInterior = e.NoInterior,
                Pais = e.Pais,
                Referencias = e.Referencias

            }).Where(t2 => direccionesempresa.Where(d => d.IDEmpresa == idempresa).Any(d1 => t2.IDDireccion == d1.IDDireccion)).ToList();
        }

        // GET: api/Empresas/Select
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> Select(int id)
        {
            if (id == 0)
            {
                var empresa = await _context.Empresas.ToListAsync();

                empresa.Add(new Empresa()
                {
                    IDEmpresa = 0,
                    NombreComercial = "Root"
                });

                return empresa.Select(e => new SelectViewModel
                {
                    IDEmpresa = e.IDEmpresa,
                    NombreComercial = e.NombreComercial
                });
            }
            else
            {
                var empresa = await _context.Empresas.Where(_ => _.IDEmpresa == id).ToListAsync();

                if (empresa != null)
                {
                    return empresa.Select(e => new SelectViewModel
                    {
                        IDEmpresa = e.IDEmpresa,
                        NombreComercial = e.NombreComercial
                    });
                }
            }

            var aux = new List<SelectViewModel>();
            return aux.Select(e => new SelectViewModel
            {
                IDEmpresa = e.IDEmpresa,
                NombreComercial = e.NombreComercial
            });

        }

        // GET: api/Empresas/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var empresa = await _context.Empresas.FindAsync(id);
            var direccionesempresa = await _context.DireccionesEmpresas.ToListAsync();
            var direcciones = await _context.Direcciones.ToListAsync();

            var telefonos = await _context.Telefonos.Include(t => t.tipotelefono).ToListAsync();
            var telefonosempresa = await _context.TelefonosEmpresas.ToListAsync();

            if (empresa == null)
            {
                return NotFound();
            }

            return Ok(new ActualizarViewModel
            {
                IDEmpresa = empresa.IDEmpresa,
                NombreComercial = empresa.NombreComercial,
                Email = empresa.Email,

                RazonSocial = empresa.RazonSocial,
                RFC = empresa.RFC,
                direcciones = direcciones.Select(e => new DireccionViewModel
                {
                    IDDireccion = e.IDDireccion,
                    Identificador = e.Identificador,
                    Calle = e.Calle,
                    IDColonia = e.IDColonia,
                    Colonia = e.Colonia,
                    IDEstado = e.IDEstado,
                    IDLocalidad = e.IDLocalidad,
                    IDMunicipio = e.IDMunicipio,
                    Localidad = e.Localidad,
                    EntreCalles = e.EntreCalles,
                    Estado = e.Estado,
                    Municipio = e.Municipio,
                    NoExterior = e.NoExterior,
                    NoInterior = e.NoInterior,
                    Pais = e.Pais,
                    Referencias = e.Referencias

                }).Where(t2 => direccionesempresa.Where(d => d.IDEmpresa == empresa.IDEmpresa).Any(d1 => t2.IDDireccion == d1.IDDireccion)).ToList(),
                telefonos = telefonos.Select(e => new TelefonoViewModel
                {
                    IDTelefono = e.IDTelefono,
                    IDTipoTelefono = e.IDTipoTelefono,
                    Lada = e.Lada,
                    NoTelefono = e.NoTelefono,
                    TipoTelefono = e.tipotelefono.Nombre
                }).Where(t2 => telefonosempresa.Where(d => d.IDEmpresa == empresa.IDEmpresa).Any(d1 => t2.IDTelefono == d1.IDTelefono)).ToList(),
            });
        }

        // GET: api/Empresas/MostrarEmpresaUsuario/5
        [HttpGet("[action]/{idusuario}/{nivel}")]
        public async Task<IActionResult> MostrarEmpresaUsuario([FromRoute] int idusuario, int nivel)
        {
            if (nivel == 2)
            {
                var administrador = await _context.Administradores.FirstOrDefaultAsync(_ => _.IDUsuario == idusuario);

                var empresa = await _context.Empresas.FindAsync(administrador.IDEmpresa);

                if (empresa == null)
                {
                    return NotFound();
                }

                return Ok(new EmpresaViewModel
                {
                    IDEmpresa = empresa.IDEmpresa,
                    NombreComercial = empresa.NombreComercial,
                    Estatus = empresa.Estatus
                });
            }

            if (nivel == 1)
            {
                var encargado = await _context.Encargados.FirstOrDefaultAsync(_ => _.IDUsuario == idusuario);

                var empresa = await _context.Empresas.FindAsync(encargado.IDEncargado);

                if (empresa == null)
                {
                    return NotFound();
                }

                return Ok(new EmpresaViewModel
                {
                    IDEmpresa = empresa.IDEmpresa,
                    NombreComercial = empresa.NombreComercial,
                    Estatus = empresa.Estatus
                });
            }

            var emp = new SelectViewModel();

            return Ok(new SelectViewModel
            {
                IDEmpresa = emp.IDEmpresa,
                NombreComercial = "Root"
            });
        }

        // POST: api/Empresas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Empresa empresa = new Empresa
            {
                Email = model.Email,
                NombreComercial = model.NombreComercial,
                RazonSocial = model.RazonSocial,
                RFC = model.RFC,
                FechaCreacion = DateTime.Today,
                Estatus = model.Estatus
            };

            _context.Empresas.Add(empresa);

            await _context.SaveChangesAsync();

            var idempresa = empresa.IDEmpresa;

            foreach (var item in model.direcciones)
            {

                Direccion direccion = new Direccion()
                {
                    Identificador = item.Identificador.Trim(),
                    Calle = item.Calle.Trim(),
                    Colonia = item.Colonia.Trim(),
                    EntreCalles = item.EntreCalles.Trim(),
                    IDColonia = item.IDColonia,
                    IDEstado = item.IDEstado,
                    IDLocalidad = item.IDLocalidad,
                    IDMunicipio = item.IDMunicipio,
                    Estado = item.Estado.Trim(),
                    Localidad = item.Localidad.Trim(),
                    Municipio = item.Municipio.Trim(),
                    NoExterior = item.NoExterior.Trim(),
                    NoInterior = item.NoInterior.Trim(),
                    Pais = item.Pais.Trim(),
                    Referencias = item.Referencias.Trim()
                };

                _context.Direcciones.Add(direccion);
                await _context.SaveChangesAsync();

                var iddireccion = direccion.IDDireccion;
                DireccionEmpresa direccionEmpresa = new DireccionEmpresa()
                {
                    IDDireccion = iddireccion,
                    IDEmpresa = idempresa
                };

                _context.DireccionesEmpresas.Add(direccionEmpresa);

                await _context.SaveChangesAsync();
            }

            foreach (var item in model.telefonos)
            {
                Telefono telefono = new Telefono()
                {
                    IDTipoTelefono = item.IDTipoTelefono,
                    Lada = item.Lada,
                    NoTelefono = item.NoTelefono
                };

                _context.Telefonos.Add(telefono);
                await _context.SaveChangesAsync();

                var idtelefono = telefono.IDTelefono;
                TelefonoEmpresa telefonoEmpresa = new TelefonoEmpresa()
                {
                    IDTelefono = idtelefono,
                    IDEmpresa = idempresa
                };

                _context.TelefonosEmpresas.Add(telefonoEmpresa);

                await _context.SaveChangesAsync();
            }

            if (empresa.Estatus == null)
            {
                EmailService service = new EmailService();

                ActivationCompanyRequest activationCompanyRequest = new ActivationCompanyRequest()
                {
                    NombreComercial = empresa.NombreComercial,
                    RazonSocial = empresa.RazonSocial,
                    RFC = empresa.RFC
                };
                await service.SendActivationCompanyRequest(activationCompanyRequest);
            }

            return Ok(empresa);
        }

        // PUT: api/Empresas/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.IDEmpresa <= 0)
            {
                return BadRequest();
            }

            var empresa = await _context.Empresas.FirstOrDefaultAsync(c => c.IDEmpresa == model.IDEmpresa);

            if (empresa == null)
            {
                return NotFound();
            }

            empresa.NombreComercial = model.NombreComercial;
            empresa.RazonSocial = model.RazonSocial;
            empresa.RFC = model.RFC;
            empresa.Email = model.Email;

            try
            {
                await _context.SaveChangesAsync();

                foreach (var dir in model.direcciones)
                {
                    //Eliminar
                    if (dir.Origen == "eliminar")
                    {
                        var direccion = await _context.Direcciones.FirstOrDefaultAsync(t => t.IDDireccion == dir.IDDireccion);

                        if (direccion == null) { return NotFound(); }

                        var dirempresa = await _context.DireccionesEmpresas.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                        _context.Direcciones.Remove(direccion);
                        await _context.SaveChangesAsync();

                        _context.DireccionesEmpresas.Remove(dirempresa);
                        await _context.SaveChangesAsync();


                    }

                    //Agregar
                    if (dir.Origen == "Nuevo")
                    {
                        Direccion direccion = new Direccion()
                        {
                            Identificador = dir.Identificador,
                            Calle = dir.Calle,
                            Colonia = dir.Colonia,
                            EntreCalles = dir.EntreCalles,
                            IDColonia = dir.IDColonia,
                            IDEstado = dir.IDEstado,
                            IDLocalidad = dir.IDLocalidad,
                            IDMunicipio = dir.IDMunicipio,
                            Estado = dir.Estado,
                            Localidad = dir.Localidad,
                            Municipio = dir.Municipio,
                            NoExterior = dir.NoExterior,
                            NoInterior = dir.NoInterior,
                            Pais = dir.Pais,
                            Referencias = dir.Referencias
                        };

                        _context.Direcciones.Add(direccion);
                        await _context.SaveChangesAsync();

                        var iddireccion = direccion.IDDireccion;
                        DireccionEmpresa direccionEmpresa = new DireccionEmpresa()
                        {
                            IDDireccion = iddireccion,
                            IDEmpresa = empresa.IDEmpresa
                        };

                        _context.DireccionesEmpresas.Add(direccionEmpresa);

                        await _context.SaveChangesAsync();
                    }

                    //Editar
                    if (dir.Origen == "Editado")
                    {
                        var direccion = await _context.Direcciones.FirstOrDefaultAsync(i => i.IDDireccion == dir.IDDireccion);

                        direccion.IDColonia = dir.IDColonia;
                        direccion.Identificador = dir.Identificador;
                        direccion.IDEstado = dir.IDEstado;
                        direccion.IDLocalidad = dir.IDLocalidad;
                        direccion.IDMunicipio = dir.IDMunicipio;
                        direccion.Localidad = dir.Localidad;
                        direccion.Municipio = dir.Municipio;
                        direccion.NoExterior = dir.NoExterior;
                        direccion.NoInterior = dir.NoInterior;
                        direccion.Pais = dir.Pais;
                        direccion.Referencias = dir.Referencias;
                        direccion.Estado = dir.Estado;
                        dir.EntreCalles = dir.EntreCalles;
                        dir.Colonia = dir.Colonia;
                        dir.Calle = dir.Calle;

                        await _context.SaveChangesAsync();
                    }
                }

                foreach (var tel in model.telefonos)
                {
                    //Eliminar 
                    if (tel.Origen == "eliminar")
                    {
                        var telefono = await _context.Telefonos.FirstOrDefaultAsync(t => t.IDTelefono == tel.IDTelefono);

                        if (telefono == null) { return NotFound(); }

                        var telempresa = await _context.TelefonosEmpresas.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                        _context.TelefonosEmpresas.Remove(telempresa);

                        await _context.SaveChangesAsync();

                        _context.Telefonos.Remove(telefono);
                        await _context.SaveChangesAsync();

                    }

                    //Agregar
                    if (tel.Origen == "Nuevo")
                    {
                        Telefono telefono = new Telefono()
                        {
                            IDTipoTelefono = tel.IDTipoTelefono,
                            Lada = tel.Lada,
                            NoTelefono = tel.NoTelefono
                        };

                        _context.Telefonos.Add(telefono);
                        await _context.SaveChangesAsync();

                        var idtelefono = telefono.IDTelefono;
                        TelefonoEmpresa telefonoEmpresa = new TelefonoEmpresa()
                        {
                            IDTelefono = idtelefono,
                            IDEmpresa = empresa.IDEmpresa
                        };

                        _context.TelefonosEmpresas.Add(telefonoEmpresa);

                        await _context.SaveChangesAsync();
                    }

                    //Editar
                    if (tel.Origen == "Editado")
                    {
                        var telefono = await _context.Telefonos.FirstOrDefaultAsync(i => i.IDTelefono == tel.IDTelefono);

                        telefono.Lada = tel.Lada;
                        telefono.NoTelefono = tel.NoTelefono;
                        telefono.IDTipoTelefono = tel.IDTipoTelefono;

                        await _context.SaveChangesAsync();
                    }

                }


            }
            catch (Exception)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        //PUT: api/Empresas/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var empresa = await _context.Empresas.FirstOrDefaultAsync(e => e.IDEmpresa == id);

            if (empresa == null)
            {
                return NotFound();
            }

            empresa.Estatus = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Empresas/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var empresa = await _context.Empresas.FirstOrDefaultAsync(e => e.IDEmpresa == id);

            if (empresa == null)
            {
                return NotFound();
            }

            empresa.Estatus = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        private bool EmpresaExists(int id)
        {
            return _context.Empresas.Any(e => e.IDEmpresa == id);
        }
    }
}
