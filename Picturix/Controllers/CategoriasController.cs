﻿using Data;
using Entities.Categorias;
using Entities.Fotos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models.Categorias;
using Web.Services.Service;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class CategoriasController : ControllerBase
    {
        private readonly DbContextPicturix _context;

        public CategoriasController(DbContextPicturix context) { _context = context; }

        // GET: api/Categorias/Listar/1
        [HttpGet("[action]/{idEmpresa}")]
        public async Task<IEnumerable<FileDriveViewModel>> Listar([FromRoute] int idEmpresa)
        {
            // get categories and photos from DB
            List<Categoria> categorias = await _context.Categorias.ToListAsync();
            List<Foto> fotos = await _context.Fotos.ToListAsync();
            List<Foto> principal_photos = fotos.FindAll(_ => _.Nombre.Contains("[001]"));
            List<Foto> secondary_photos = fotos.FindAll(_ => !_.Nombre.Contains("[001]"));

            //validate company
            if (idEmpresa > 0)
            {

                //get company categories prices
                foreach (var item in categorias)
                {
                    var precio_categoria = await _context.CategoriasPrecio.FirstOrDefaultAsync(_ => _.IDEmpresa == idEmpresa && _.IDCategoria == item.IdCategoria);
                    if (precio_categoria != null)
                        item.Precio = precio_categoria.Precio;
                }

                //get company photos prices principal
                foreach (var item in principal_photos)
                {
                    var precio_foto = await _context.FotosPrecio.FirstOrDefaultAsync(_ => _.IDEmpresa == idEmpresa && _.IDFoto == item.IDFoto);
                    if (precio_foto != null)
                        item.Precio = precio_foto.Precio;

                }

                //get company photos prices secondary
                foreach (var item in principal_photos)
                {
                    var precio_foto = await _context.FotosPrecio.FirstOrDefaultAsync(_ => _.IDEmpresa == idEmpresa && _.IDFoto == item.IDFoto);
                    if (precio_foto != null)
                        item.Precio = precio_foto.Precio;

                }

            }

            //mapping categories to filedrive
            List<FileDriveViewModel> folders = categorias.Select(t => new FileDriveViewModel
            {
                id = t.IdCategoria,
                estatus = t.Estatus,
                nombre = t.Nombre,
                padre = t.Padre,
                nivel = 0,
                idgoogledrive = t.IdGoogleDrive,
                children = new List<FileDriveViewModel>(),
                isfile = false,
                urlgoogledrive = "https://" + "drive" + ".google.com/uc?export=view&id=" + t.IdGoogleDrive,
                precio = t.Precio
            }).ToList();

            //create tree directory(only folders)
            var treefolders = FillRecursive(folders.OrderBy(_ => _.id).ToList(), -1)[0];

            List<FileDriveViewModel> directory = new List<FileDriveViewModel>();

            //assing levels to items tree (0 root,1 providers,2 categories,3 subcategories)
            directory.Add(AssingLevelsTree(treefolders, folders));

            //add principal photos to tree structure 
            directory[0] = GetFiles(directory[0], principal_photos.OrderBy(_ => _.IDCategoria).ToList());

            //add secondary photos to tree structure
            directory.Add(AddSecondaryPhotos(secondary_photos));

            return directory.Select(t => new FileDriveViewModel
            {
                id = t.id,
                nombre = t.nombre,
                estatus = t.estatus,
                nivel = t.nivel,
                padre = t.padre,
                children = t.children,
                isfile = t.isfile,
                precio = t.precio,
                urlgoogledrive = t.urlgoogledrive
            });
        }

        private static FileDriveViewModel AddSecondaryPhotos(List<Foto> photos)
        {
            FileDriveViewModel tree = new FileDriveViewModel();

            foreach (var item in photos)
            {
                //remove extension of name file 
                var bracket_pos = item.Nombre.IndexOf('[');
                var nombre = string.Empty;

                if (bracket_pos > -1)
                    nombre = item.Nombre.Substring(0, bracket_pos);
                else
                    nombre = item.Nombre;

                FileDriveViewModel _FD = new FileDriveViewModel()
                {
                    id = item.IDFoto,
                    idgoogledrive = item.IDGoogleDrive,
                    estatus = true,
                    isfile = true,
                    nivel = 0,
                    nombre = nombre,
                    precio = item.Precio,
                    urlgoogledrive = item.UrlGoogleDrive,
                    children = new List<FileDriveViewModel>(),
                    padre = item.IDCategoria,
                };

                tree.children.Add(_FD);
            }

            return tree;
        }

        private static FileDriveViewModel AssingLevelsTree(FileDriveViewModel tree, List<FileDriveViewModel> categorias)
        {

            foreach (var item in categorias)
            {
                if (tree.id == item.id)
                    tree.nivel = 0;


                foreach (var proveedor in tree.children)
                {
                    if (proveedor.id == item.id)
                        proveedor.nivel = 1;

                    foreach (var categoria in proveedor.children)
                    {
                        if (categoria.id == item.id)
                            categoria.nivel = 2;

                        foreach (var subcategoria in categoria.children)
                            if (subcategoria.id == item.id)
                                subcategoria.nivel = 3;

                    }
                }

            }

            return tree;
        }

        private static List<FileDriveViewModel> FillRecursive(List<FileDriveViewModel> flatObjects, int? parentId = null)
        {
            return flatObjects.Where(x => x.padre.Equals(parentId)).Select(item => new FileDriveViewModel
            {
                id = item.id,
                estatus = item.estatus,
                nombre = item.nombre,
                padre = item.padre,
                nivel = flatObjects.Find(x => x.padre.Equals(parentId)).nivel + 1 ?? 0,
                children = FillRecursive(flatObjects, item.id),
                isfile = item.isfile,
                precio = item.precio,
                urlgoogledrive = item.urlgoogledrive,
                idgoogledrive = item.idgoogledrive
            }).ToList();
        }

        private static FileDriveViewModel GetFiles(FileDriveViewModel tree, List<Foto> fotos)
        {
            FileDriveViewModel auxtree = tree;

            foreach (var item in fotos)
            {
                if (tree.id == item.IDCategoria)
                {
                    //remove extension of name file 
                    var bracket_pos = item.Nombre.IndexOf('[');
                    var nombre = string.Empty;

                    if (bracket_pos > -1)
                        nombre = item.Nombre.Substring(0, bracket_pos);
                    else
                        nombre = item.Nombre;

                    FileDriveViewModel _FD = new FileDriveViewModel()
                    {
                        id = item.IDFoto,
                        idgoogledrive = item.IDGoogleDrive,
                        estatus = true,
                        isfile = true,
                        nivel = 0,
                        nombre = nombre,
                        precio = item.Precio,
                        urlgoogledrive = item.UrlGoogleDrive,
                        children = new List<FileDriveViewModel>(),
                        padre = item.IDCategoria
                    };

                    auxtree.children.Add(_FD);
                }

                foreach (var proveedor in tree.children)
                {
                    if (proveedor.id == item.IDCategoria)
                    {
                        //remove extension of name file 
                        var bracket_pos = item.Nombre.IndexOf('[');
                        var nombre = string.Empty;

                        if (bracket_pos > -1)
                            nombre = item.Nombre.Substring(0, bracket_pos);
                        else
                            nombre = item.Nombre;

                        FileDriveViewModel _FD = new FileDriveViewModel()
                        {
                            id = item.IDFoto,
                            idgoogledrive = item.IDGoogleDrive,
                            estatus = true,
                            isfile = true,
                            nivel = 1,
                            nombre = nombre,
                            precio = item.Precio,
                            urlgoogledrive = item.UrlGoogleDrive,
                            children = new List<FileDriveViewModel>(),
                            padre = item.IDCategoria
                        };

                        auxtree.children.Find(_ => _.id == proveedor.id).children.Add(_FD);
                    }

                    foreach (var categoria in proveedor.children)
                    {
                        if (categoria.id == item.IDCategoria)
                        {
                            //remove extension of name file 
                            var bracket_pos = item.Nombre.IndexOf('[');
                            var nombre = string.Empty;

                            if (bracket_pos > -1)
                                nombre = item.Nombre.Substring(0, bracket_pos);
                            else
                                nombre = item.Nombre;

                            FileDriveViewModel _FD = new FileDriveViewModel()
                            {
                                id = item.IDFoto,
                                idgoogledrive = item.IDGoogleDrive,
                                estatus = true,
                                isfile = true,
                                nivel = 2,
                                nombre = nombre,
                                precio = item.Precio,
                                urlgoogledrive = item.UrlGoogleDrive,
                                children = new List<FileDriveViewModel>(),
                                padre = item.IDCategoria
                            };

                            auxtree.children.
                            Find(_ => _.id == proveedor.id).children.
                            Find(_ => _.id == categoria.id).children.Add(_FD);

                        }

                        foreach (var subcategoria in categoria.children)
                            if (subcategoria.id == item.IDCategoria)
                            {
                                //remove extension of name file 
                                var bracket_pos = item.Nombre.IndexOf('[');
                                var nombre = string.Empty;

                                if (bracket_pos > -1)
                                    nombre = item.Nombre.Substring(0, bracket_pos);
                                else
                                    nombre = item.Nombre;

                                FileDriveViewModel _FD = new FileDriveViewModel()
                                {
                                    id = item.IDFoto,
                                    idgoogledrive = item.IDGoogleDrive,
                                    estatus = true,
                                    isfile = true,
                                    nivel = 3,
                                    nombre = nombre,
                                    precio = item.Precio,
                                    urlgoogledrive = item.UrlGoogleDrive,
                                    children = new List<FileDriveViewModel>(),
                                    padre = item.IDCategoria
                                };

                                auxtree.children.
                                Find(_ => _.id == proveedor.id).children.
                                Find(_ => _.id == categoria.id).children.
                                Find(_ => _.id == subcategoria.id).children.Add(_FD);

                            }
                    }
                }
            }

            return auxtree;
        }

        // GET: api/Categorias/Select/1
        [HttpGet("[action]/{categoriaActual}")]
        public async Task<IEnumerable<SelectViewModel>> Select([FromRoute]int categoriaActual)
        {
            var categoria = await _context.Categorias.Where(c => c.Estatus == true).ToListAsync();

            //categoria.Add(new Categoria() { IdCategoria = 0, Estatus = true, Nombre = "Ninguna", Padre = 0 });

            return categoria.Select(c => new SelectViewModel
            {
                IdCategoria = c.IdCategoria,
                Nombre = c.Nombre
            });
        }

        // GET: api/Categorias/Mostrar/1
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {

            var categoria = await _context.Categorias.FindAsync(id);

            if (categoria == null)
            {
                return NotFound();
            }

            return Ok(new CategoriaViewModel
            {
                //IdCategoria = categoria.IdCategoria,
                //Nombre = categoria.Nombre,
                //Padre = categoria.Padre,
                //Estatus = categoria.Estatus
            });
        }

        // PUT: api/Categorias/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (model.Isfile)
                {
                    if (model.Idempresa == 0)
                    {
                        Foto foto = await _context.Fotos.FirstOrDefaultAsync(c => c.IDFoto == model.Id);
                        if (foto == null)
                            return NotFound();

                        //remove extension of name file 
                        var bracket_pos = foto.Nombre.IndexOf('[');
                        var nombre = string.Empty;

                        if (bracket_pos > -1)
                            nombre = foto.Nombre.Substring(0, bracket_pos);
                        else
                            nombre = foto.Nombre;

                        List<Foto> fotos = _context.Fotos.Where(c => c.IDCategoria == foto.IDCategoria && c.Nombre.Substring(0, bracket_pos) == nombre && c.IDCategoria == model.Padre).ToList();

                        foreach (var item in fotos)
                        {
                            var fotoactualizar = await _context.Fotos.FirstOrDefaultAsync(c => c.IDFoto == item.IDFoto);
                            fotoactualizar.Precio = model.Precio;
                            await _context.SaveChangesAsync();
                        }

                    }
                    else
                    {
                        Foto foto = await _context.Fotos.FirstOrDefaultAsync(c => c.IDFoto == model.Id);
                        if (foto == null)
                            return NotFound();

                        //remove extension of name file 
                        var bracket_pos = foto.Nombre.IndexOf('[');
                        var nombre = string.Empty;

                        if (bracket_pos > -1)
                            nombre = foto.Nombre.Substring(0, bracket_pos);
                        else
                            nombre = foto.Nombre;

                        List<Foto> fotos = _context.Fotos.Where(c => c.IDCategoria == foto.IDCategoria && c.Nombre.Substring(0, bracket_pos) == nombre && c.IDCategoria == model.Padre).ToList();

                        foreach (var item in fotos)
                        {
                            var fotoactualizar = await _context.FotosPrecio.FirstOrDefaultAsync(c => c.IDFoto == item.IDFoto && c.IDEmpresa == model.Idempresa);

                            if (fotoactualizar == null)
                            {
                                FotoPrecio nuevo = new FotoPrecio()
                                {
                                    IDEmpresa = model.Idempresa,
                                    IDFoto = model.Id,
                                    Precio = model.Precio
                                };

                                _context.FotosPrecio.Add(nuevo);
                                await _context.SaveChangesAsync();
                            }
                            else
                            {

                                fotoactualizar.Precio = model.Precio;
                                await _context.SaveChangesAsync();
                            }

                        }

                    }
                }
                else
                {
                    var categoria = await _context.Categorias.FirstOrDefaultAsync(c => c.IdCategoria == model.Id);

                    if (categoria == null)
                        return NotFound();

                    if (model.Idempresa == 0)
                    {
                        GoogleDriveService _gService = new GoogleDriveService();
                        var idGoogleDrive = categoria.IdGoogleDrive;

                        if (categoria.Nombre != model.Nombre || categoria.Padre != model.Padre)
                        {
                            if (categoria.Padre != model.Padre)
                            {
                                var cat = await _context.Categorias.FirstOrDefaultAsync(_ => _.IdCategoria == model.Padre);
                                _gService.UpdateFolder(false, categoria.IdGoogleDrive, string.Empty, true, cat.IdGoogleDrive);
                                idGoogleDrive = _gService.IdParent;
                            }
                            else
                            {
                                _gService.UpdateFolder(true, idGoogleDrive, model.Nombre, false, string.Empty);
                            }
                        }

                        categoria.Nombre = model.Nombre;
                        categoria.Padre = model.Padre;
                        categoria.Precio = model.Precio;
                        categoria.IdGoogleDrive = idGoogleDrive;
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        var categoria_precio = await _context.CategoriasPrecio.FirstOrDefaultAsync(_ => _.IDCategoria == model.Id && _.IDEmpresa == model.Idempresa);

                        if (categoria_precio == null)
                        {
                            CategoriaPrecio nuevo = new CategoriaPrecio()
                            {
                                IDEmpresa = model.Idempresa,
                                Precio = model.Precio,
                                IDCategoria = model.Id
                            };

                            _context.CategoriasPrecio.Add(nuevo);
                        }
                        else
                        {
                            categoria_precio.Precio = model.Precio;
                        }

                        await _context.SaveChangesAsync();
                    }
                }

                return Ok();

            }
            catch (Exception)
            {

                // Guardar Excepción
                return BadRequest();
            }
        }

        // POST: api/Categorias/ActualizarDirectorio
        [HttpPost("[action]")]
        public async Task<IActionResult> ActualizarDirectorio([FromBody] ActualizarDirectorioViewModel folder)
        {
            try
            {
                List<Categoria> categorias = await _context.Categorias.ToListAsync();
                Categoria categoria = categorias.ToList().Find(_ => _.IdCategoria == folder.IdCategoria);

                if (categoria != null)
                {
                    List<FileDriveViewModel> folders = categorias.Select(t => new FileDriveViewModel
                    {
                        idgoogledrive = t.IdGoogleDrive,
                        id = t.IdCategoria,
                        estatus = t.Estatus,
                        nombre = t.Nombre,
                        padre = t.Padre,
                        nivel = 0,
                        children = new List<FileDriveViewModel>(),
                        isfile = false,
                        urlgoogledrive = "https://" + "drive" + ".google.com/uc?export=view&id=" + t.IdGoogleDrive,
                        precio = t.Precio
                    }).ToList();

                    var treefolders = FillRecursive(folders.OrderBy(_ => _.id).ToList(), -1)[0];
                    List<string> parents = GetParentsFolders(treefolders, categoria.IdGoogleDrive, folder.Nivel);

                    var fotos = await _context.Fotos.ToListAsync();
                    GoogleDriveService _gService = new GoogleDriveService();
                    var googlefotos = await _gService.GetFiles(categoria.IdGoogleDrive, parents);

                    //Buscar fotos existentes en BD y en google para comprobar que la carpeta contenedora y el nombre estan actualizados
                    List<Google.Apis.Drive.v3.Data.File> fotosexistentes = googlefotos.Where(p => fotos.Any(p2 => p2.IDGoogleDrive == p.Id)).ToList();

                    for (int i = 0; i < fotosexistentes.Count; i++)
                        if (_context.Categorias.Where(_ => _.IdGoogleDrive == fotosexistentes[i].Parents[0]).ToList()[0] != null)
                        {
                            var idcategoria = _context.Categorias.Where(_ => _.IdGoogleDrive == fotosexistentes[i].Parents[0]).ToList()[0].IdCategoria;
                            var fotonombre = googlefotos.FirstOrDefault(_ => _.Id == fotosexistentes[i].Id).Name;
                            var foto = fotos.FirstOrDefault(_ => _.IDGoogleDrive == fotosexistentes[i].Id);

                            if (foto != null)
                            {
                                if (foto.IDCategoria != idcategoria || foto.Nombre != fotonombre)
                                {
                                    foto.IDCategoria = idcategoria;
                                    foto.Nombre = fotonombre;
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }

                    //Buscar fotos existentes que no se encuentran en la BD
                    List<Google.Apis.Drive.v3.Data.File> fotosinexistentesBD = googlefotos.Where(p => !fotos.Any(p2 => p2.IDGoogleDrive == p.Id)).ToList();

                    foreach (var item in fotosinexistentesBD)
                        if (_context.Fotos.FirstOrDefaultAsync(_ => _.IDGoogleDrive == item.Id) != null)
                        {
                            var idfolder = item.Parents[0];

                            if (idfolder != null)
                            {
                                var idcategoria = _context.Categorias.First(_ => _.IdGoogleDrive == idfolder).IdCategoria;

                                Foto _Foto = new Foto()
                                {
                                    IDCategoria = idcategoria,
                                    Precio = 0,
                                    IDGoogleDrive = item.Id,
                                    Nombre = item.Name,
                                    UrlGoogleDrive = "https://" + "drive" + ".google.com/uc?export=view&id=" + item.Id
                                };

                                _context.Fotos.Add(_Foto);
                                await _context.SaveChangesAsync();
                            }
                        }

                    //Buscar fotos existentes en BD que no se encuentran en el drive
                    List<Foto> fotoseliminadasBD = fotos.Where(p => !googlefotos.Any(p2 => p2.Id == p.IDGoogleDrive)).ToList();

                    foreach (var item in fotoseliminadasBD)
                        if (_context.Fotos.Find(item.IDFoto) != null)
                        {
                            var foto = _context.Fotos.Find(item.IDFoto);

                            _context.Fotos.Remove(foto);
                            await _context.SaveChangesAsync();
                        }

                }
                return Ok();
            }
            catch (Exception x)
            {
                //"System.Net.Http.WinHttpException(0x80072EE2)"
                string message = x.Message;
                return BadRequest();
            }
        }

        private List<string> GetParentsFolders(FileDriveViewModel tree, string idgoogledrive, int nivel)
        {
            List<string> parents = new List<string>();
            parents.Add(idgoogledrive);

            switch (nivel)
            {
                case 0:

                    foreach (var proveedor in tree.children)
                    {
                        parents.Add(proveedor.idgoogledrive);

                        foreach (var categoria in proveedor.children)
                        {
                            parents.Add(categoria.idgoogledrive);

                            foreach (var subcategoria in categoria.children)
                                parents.Add(subcategoria.idgoogledrive);
                        }
                    }

                    break;
                case 1:

                    foreach (var proveedor in tree.children)
                        if (proveedor.idgoogledrive == idgoogledrive)
                        {
                            foreach (var categoria in proveedor.children)
                            {
                                parents.Add(categoria.idgoogledrive);

                                foreach (var subcategoria in categoria.children)
                                    parents.Add(subcategoria.idgoogledrive);
                            }
                            break;
                        }


                    break;
                case 2:

                    foreach (var proveedor in tree.children)
                        foreach (var categoria in proveedor.children)
                        {
                            if (categoria.idgoogledrive == idgoogledrive)
                            {
                                //parents.Add(categoria.idgoogledrive);

                                foreach (var subcategoria in categoria.children)
                                    parents.Add(subcategoria.idgoogledrive);
                                break;
                            }
                        }

                    break;
            }
            return parents;
        }

        // POST: api/Categorias/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            string padre = string.Empty;

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                if (model.Padre > 0)
                {
                    var categoriapadre = await _context.Categorias.FirstOrDefaultAsync(_ => _.IdCategoria == model.Padre);
                    padre = categoriapadre.IdGoogleDrive;
                }

                GoogleDriveService _gService = new GoogleDriveService();

                if (_gService.CreateFolder(model.Nombre, padre))
                {
                    var IdGoogleDrive = _gService.IdFile;

                    Categoria categoria = new Categoria
                    {
                        Nombre = model.Nombre,
                        Padre = model.Padre,
                        IdGoogleDrive = IdGoogleDrive,
                        Precio = model.Precio,
                        Estatus = true
                    };

                    _context.Categorias.Add(categoria);

                    await _context.SaveChangesAsync();

                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        private bool CategoriaExists(int id)
        {
            return _context.Categorias.Any(e => e.IdCategoria == id);
        }
    }
}
