﻿using Data;
using Entities.Empresas;
using Entities.Usuarios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Web.Models.Email;
using Web.Models.Usuarios;
using Web.Services;

namespace Web.Controllers
{
    [Route("api/[controller]/")]
    public class UsuariosController : Controller
    {
        private readonly DbContextPicturix _context;
        private readonly IConfiguration _config;

        public UsuariosController(DbContextPicturix context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/Usuarios/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuarioViewModel>> Listar()
        {
            var usuario = await _context.Usuarios.Include(u => u.IDPerfil).ToListAsync();

            return usuario.Select(u => new UsuarioViewModel
            {
                IDUsuario = u.IDUsuario,
                IDPerfil = u.IDPerfil,
                Nombre = u.Nombre,
                Password_hash = u.Password_hash,
                Estatus = u.Estatus
            });
        }

        // GET: api/Usuarios/ListarAvanzado
        [HttpGet("[action]/{idempresa}/{idusuarioactual}")]
        public async Task<IEnumerable<UsuarioViewModel>> ListarAvanzado([FromQuery] int idempresa, int idusuarioactual)
        {
            if (idempresa == 0)
            {
                var usuario = await _context.Usuarios.Include(u => u.perfil).Where(_ => _.IDUsuario != idusuarioactual && _.perfil.Nivel == 3).ToListAsync();

                return usuario.Select(u => new UsuarioViewModel
                {
                    IDUsuario = u.IDUsuario,
                    IDPerfil = u.IDPerfil,
                    NombrePerfil = u.perfil.Nombre,
                    Nombre = u.Nombre,
                    Estatus = u.Estatus
                });
            }
            else
            {
                var usuarioempresa = await _context.UsuariosEmpresa.Include(i => i.usuario).Include(i => i.usuario.perfil).Where(_ => _.IDUsuario != idusuarioactual).ToListAsync();

                return usuarioempresa.Select(u => new UsuarioViewModel
                {
                    IDUsuario = u.IDUsuario,
                    IDPerfil = u.usuario.IDPerfil,
                    Nombre = u.usuario.perfil.Nombre,
                    Estatus = u.usuario.Estatus
                });
            }
        }

        // GET: api/Usuarios/Mostrar/5
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(new MostrarViewModel
            {
                IDPerfil = usuario.IDPerfil,
                IDUsuario = usuario.IDUsuario,
                Usuario = usuario.Nombre
            });
        }

        // POST: api/Usuarios/Crear
        [HttpPost]
        [Route("Crear")]
        public async Task<IActionResult> Crear([FromBody] Models.Usuarios.CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var username = model.Nombre.ToLower();

            if (await _context.Usuarios.AnyAsync(u => u.Nombre == username))
            {
                return BadRequest("El usuario ya existe");
            }

            CrearPasswordHash(model.Password, out byte[] passwordHash, out byte[] passwordSalt);

            Usuario usuario = new Usuario
            {
                IDPerfil = model.IDPerfil,
                Nombre = model.Nombre,
                Password_hash = passwordHash,
                Password_salt = passwordSalt,
                Estatus = true
            };

            _context.Usuarios.Add(usuario);

            try
            {
                await _context.SaveChangesAsync();

                EmailService service = new EmailService();

                AccesCredentialsViewModel accesCredentials = new AccesCredentialsViewModel()
                {
                    Usuario = usuario.Nombre,
                    Contraseña = model.Password,
                    Email = model.Email
                };
                await service.SendInitialAccessCredentials(accesCredentials);
            }
            catch (System.Exception)
            {
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Usuarios/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (model.IDUsuario <= 0)
                {
                    return BadRequest();
                }

                var usuario = await _context.Usuarios.FindAsync(model.IDUsuario);

                usuario.IDPerfil = model.IDPerfil;

                if (!string.IsNullOrEmpty(model.Password))
                {
                    CrearPasswordHash(model.Password, out byte[] passwordHash, out byte[] passwordSalt);
                    usuario.Password_hash = passwordHash;
                    usuario.Password_salt = passwordSalt;
                }

                await _context.SaveChangesAsync();

                if (!string.IsNullOrEmpty(model.Password))
                {
                    EmailService service = new EmailService();

                    AccesCredentialsViewModel accesCredentials = new AccesCredentialsViewModel()
                    {
                        Usuario = usuario.Nombre,
                        Contraseña = model.Password,
                        Email = model.Email
                    };
                    await service.SendInitialAccessCredentials(accesCredentials);
                }

            }
            catch (Exception)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/Usuarios/CrearUsuarioEmpresa
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearUsuarioEmpresa([FromBody] CrearUsuarioEmpresaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var username = model.Nombre.ToLower();

            if (await _context.Usuarios.AnyAsync(u => u.Nombre == username))
            {
                return BadRequest("El usuario ya existe");
            }

            CrearPasswordHash(model.Password, out byte[] passwordHash, out byte[] passwordSalt);

            Usuario usuario = new Usuario
            {
                IDPerfil = model.IDPerfil,
                Nombre = model.Nombre,
                Password_hash = passwordHash,
                Password_salt = passwordSalt,
                Estatus = true
            };

            _context.Usuarios.Add(usuario);

            try
            {
                await _context.SaveChangesAsync();

                var user = await _context.Usuarios.Include(_ => _.perfil).FirstOrDefaultAsync(_ => _.perfil.IDPerfil == usuario.IDPerfil);

                if (user.perfil.Nivel < 3)
                {
                    UsuarioEmpresa usuarioEmpresa = new UsuarioEmpresa()
                    {
                        IDEmpresa = model.IDEmpresa,
                        IDUsuario = usuario.IDUsuario
                    };

                    _context.UsuariosEmpresa.Add(usuarioEmpresa);
                    await _context.SaveChangesAsync();
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
            }

            return Ok();
        }

        //PUT: api/Usuarios/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.Usuarios.FirstOrDefaultAsync(e => e.IDUsuario == id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.Estatus = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Usuarios/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.Usuarios.FirstOrDefaultAsync(e => e.IDUsuario == id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.Estatus = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                //Guardar Exception
                return BadRequest();
            }

            return Ok();
        }

        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        // POST: api/Usuarios/Login
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            var nombre = model.Nombre.ToLower();

            var usuario = await _context.Usuarios.Where(u => u.Estatus == true).Include(u => u.perfil).FirstOrDefaultAsync(u => u.Nombre == nombre);

            if (usuario == null)
            {
                return NotFound();
            }

            if (!VerificarPasswordHash(model.Password, usuario.Password_hash, usuario.Password_salt))
            {
                return NotFound();
            }

            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, usuario.IDUsuario.ToString()),
                    new Claim(ClaimTypes.Role, usuario.perfil.Nivel.ToString()),
                    new Claim("idusuario", usuario.IDUsuario.ToString() ),
                    new Claim("rol", usuario.perfil.Nivel.ToString()),
                    new Claim("nombre", usuario.Nombre)
                };

            return Ok(
                    new { token = GenerarToken(claims) }
                );
        }

        private bool VerificarPasswordHash(string password, byte[] passwordHashAlmacenado, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var passwordHashNuevo = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return new ReadOnlySpan<byte>(passwordHashAlmacenado).SequenceEqual(new ReadOnlySpan<byte>(passwordHashNuevo));
            }
        }

        private string GenerarToken(List<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
              _config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds,
              claims: claims);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        // GET: api/Usuarios/ValidarDisponibilidad
        [HttpGet("[action]")]
        public async Task<IActionResult> ValidarDisponibilidad([FromQuery] string nombreusuario)
        {
            var username = nombreusuario.ToLower();

            if (await _context.Usuarios.AnyAsync(u => u.Nombre == username))
            {
                return Ok("No disponible");
            }
            return Ok("disponible");
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuarios.Any(e => e.IDUsuario == id);
        }
    }
}
