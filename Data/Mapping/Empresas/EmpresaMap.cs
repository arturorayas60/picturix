﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Entities.Empresas;
using Microsoft.EntityFrameworkCore;

namespace Data.Mapping.Empresas
{
    public class EmpresaMap : IEntityTypeConfiguration<Empresa>
    {
        public void Configure(EntityTypeBuilder<Empresa> builder)
        {
            builder.ToTable("Empresas").HasKey(c => c.IDEmpresa);

        }
    }
}
