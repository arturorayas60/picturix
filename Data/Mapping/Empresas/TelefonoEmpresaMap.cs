﻿using Entities.Empresas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Empresas
{
    public class TelefonoEmpresaMap : IEntityTypeConfiguration<TelefonoEmpresa>
    {
        public void Configure(EntityTypeBuilder<TelefonoEmpresa> builder)
        {
            builder.ToTable("TelefonosEmpresas")
                 .HasKey(d => d.IDTelefonoEmpresa);
        }
    }
}
