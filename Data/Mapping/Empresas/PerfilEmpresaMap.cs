﻿using Entities.Empresas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Empresas
{
    public class PerfilEmpresaMap : IEntityTypeConfiguration<PerfilEmpresa>
    {
        public void Configure(EntityTypeBuilder<PerfilEmpresa> builder)
        {
            builder.ToTable("PerfilesEmpresas")
                 .HasKey(d => d.IDPerfilEmpresa);
        }
    }
}
