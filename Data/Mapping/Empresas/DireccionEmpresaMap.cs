﻿using Entities.Empresas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mapping.Empresas
{
    public class DireccionEmpresaMap: IEntityTypeConfiguration<DireccionEmpresa>
    {
        public void Configure(EntityTypeBuilder<DireccionEmpresa> builder)
        {
            builder.ToTable("DireccionesEmpresas")
                 .HasKey(d => d.IDDireccionEmpresa);
        }
    }
}
