﻿using Entities.Administradores;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Administradores
{
    public class TelefonoAdministradorMap : IEntityTypeConfiguration<TelefonoAdministrador>
    {
        public void Configure(EntityTypeBuilder<TelefonoAdministrador> builder)
        {
            builder.ToTable("TelefonosAdministradores").HasKey(t => t.IDTelefonoAdministrador);
        }
    }
}
