﻿using Entities.Administradores;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Administradores
{
    public class DireccionAdministradorMap : IEntityTypeConfiguration<DireccionAdministrador>
    {
        public void Configure(EntityTypeBuilder<DireccionAdministrador> builder)
        {
            builder.ToTable("DireccionesAdministradores").HasKey(d => d.IDDireccionAdministrador);
        }
    }
}
