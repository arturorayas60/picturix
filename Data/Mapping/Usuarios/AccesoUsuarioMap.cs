﻿using Entities.Usuarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mapping.Accesos
{
    class AccesoUsuarioMap : IEntityTypeConfiguration<AccesoUsuario>
    {
        public void Configure(EntityTypeBuilder<AccesoUsuario> builder)
        {
            builder.ToTable("AccesosUsuario").HasKey(a => a.IDAccesoUsuario);
        }
    }
}
