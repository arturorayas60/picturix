﻿using Entities.Telefonos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Mapping.Telefonos
{
    class TelefonoMap : IEntityTypeConfiguration<Telefono>
    {
        public void Configure(EntityTypeBuilder<Telefono> builder)
        {
            builder.ToTable("Telefonos").HasKey(t => t.IDTelefono);
                //t => new { t.IDTelefono, t.IDTipoTelefono });
            //builder.HasOne(t => t.tipoTelefono);
        }
    }
}
