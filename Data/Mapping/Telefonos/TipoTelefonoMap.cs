﻿using Entities.Telefonos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Telefonos
{
    class TipoTelefonoMap : IEntityTypeConfiguration<TipoTelefono>
    {
        public void Configure(EntityTypeBuilder<TipoTelefono> builder)
        {
            builder.ToTable("TiposTelefonos").HasKey(p => p.IDTipoTelefono);
        }
    }
}
