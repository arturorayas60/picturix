﻿using Entities.Categorias;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Categorias
{
    public class CategoriaPrecioMap: IEntityTypeConfiguration<CategoriaPrecio>
    {
        public void Configure(EntityTypeBuilder<CategoriaPrecio> builder)
        {
            builder.ToTable("CategororiasPrecio")
                 .HasKey(d => d.IDCategoriaPrecio);
        }
    }
}
