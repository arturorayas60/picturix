﻿using Entities.Encargados;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Encargados
{
    public class TelefonoEncargadoMap : IEntityTypeConfiguration<TelefonoEncargado>
    {
        public void Configure(EntityTypeBuilder<TelefonoEncargado> builder)
        {
            builder.ToTable("TelefonosEncargados")
                 .HasKey(d => d.IDTelefonoEncargado);
        }
    }
}
