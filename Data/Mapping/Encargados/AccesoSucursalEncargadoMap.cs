﻿using Entities.Encargados;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Encargados
{
    public class AccesoSucursalEncargadoMap : IEntityTypeConfiguration<AccesoSucursalEncargado>
    {
        public void Configure(EntityTypeBuilder<AccesoSucursalEncargado> builder)
        {
            builder.ToTable("AccesosSucursalesEncargado")
                 .HasKey(d => d.IDAccesoSucursalEncargado);
        }
    }
}
