﻿using Entities.Encargados;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Encargados
{
    public class DireccionEncargadoMap : IEntityTypeConfiguration<DireccionEncargado>
    {
        public void Configure(EntityTypeBuilder<DireccionEncargado> builder)
        {
            builder.ToTable("DireccionesEncargados")
                 .HasKey(d => d.IDDireccionEncargado);
        }
    }
}
