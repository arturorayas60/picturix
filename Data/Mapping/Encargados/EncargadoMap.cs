﻿using Entities.Encargados;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Encargados
{
    public class EncargadoMap : IEntityTypeConfiguration<Encargado>
    {
        public void Configure(EntityTypeBuilder<Encargado> builder)
        {
            builder.ToTable("Encargados")
                 .HasKey(d => d.IDEncargado );
        }
    }
}
