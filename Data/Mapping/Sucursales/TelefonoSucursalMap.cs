﻿using Entities.Sucursales;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Sucursales
{
    public class TelefonoSucursalMap : IEntityTypeConfiguration<TelefonoSucursal>
    {
        public void Configure(EntityTypeBuilder<TelefonoSucursal> builder)
        {
            builder.ToTable("TelefonosSucursales").HasKey(s => s.IDTelefonoSucursal);
        }
    }
}
