﻿using Entities.Sucursales;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Sucursales
{
   public class LicenciaMap : IEntityTypeConfiguration<Licencia>
    {
        public void Configure(EntityTypeBuilder<Licencia> builder)
        {
            builder.ToTable("Licencias").HasKey(l => l.IDLicencia);
        }
    }
}
