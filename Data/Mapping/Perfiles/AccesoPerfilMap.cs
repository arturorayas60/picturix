﻿using Entities.Perfiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Perfiles
{
    public class AccesoPerfilMap : IEntityTypeConfiguration<AccesoPerfil>
    {
        public void Configure(EntityTypeBuilder<AccesoPerfil> builder)
        {
            builder.ToTable("AccesosPerfiles").HasKey(p => p.IDAccesoPerfil);
        }
    }
}
