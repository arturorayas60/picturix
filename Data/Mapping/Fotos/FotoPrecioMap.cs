﻿using Entities.Fotos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Mapping.Fotos
{
    public class FotoPrecioMap : IEntityTypeConfiguration<FotoPrecio>
    {
        public void Configure(EntityTypeBuilder<FotoPrecio> builder)
        {
            builder.ToTable("FotosPrecio")
                 .HasKey(d => d.IDFotoPrecio);
        }
    }
}
