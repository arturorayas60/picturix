﻿using Data.Mapping.Accesos;
using Data.Mapping.Administradores;
using Data.Mapping.Categorias;
using Data.Mapping.Direcciones;
using Data.Mapping.Empresas;
using Data.Mapping.Encargados;
using Data.Mapping.Fotos;
using Data.Mapping.Modulos;
using Data.Mapping.Perfiles;
using Data.Mapping.Personas;
using Data.Mapping.Sucursales;
using Data.Mapping.Telefonos;
using Data.Mapping.Usuarios;
using Entities.Administradores;
using Entities.Categorias;
using Entities.Direcciones;
using Entities.Empresas;
using Entities.Encargados;
using Entities.Fotos;
using Entities.Modulos;
using Entities.Perfiles;
using Entities.Personas;
using Entities.Sucursales;
using Entities.Telefonos;
using Entities.Usuarios;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class DbContextPicturix : DbContext
    {
        public DbSet<AccesoUsuario> AccesosUsuario { get; set; }
        public DbSet<DireccionAdministrador> DireccionesAdministradores { get; set; }
        public DbSet<TelefonoAdministrador> TelefonosAdministradores { get; set; }
        public DbSet<Administrador> Administradores { get; set; }
        public DbSet<AccesoPerfil> AccesosPerfiles { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<CategoriaPrecio> CategoriasPrecio { get; set; }
        public DbSet<Foto> Fotos { get; set; }
        public DbSet<FotoPrecio> FotosPrecio { get; set; }
        public DbSet<DireccionEmpresa> DireccionesEmpresas { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<PerfilEmpresa> PerfilesEmpresas { get; set; }
        public DbSet<Modulo> Modulos { get; set; }
        public DbSet<Perfil> Perfiles { get; set; }
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<TelefonoEmpresa> TelefonosEmpresas { get; set; }
        public DbSet<Telefono> Telefonos { get; set; }
        public DbSet<TipoTelefono> TiposTelefonos { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }
        public DbSet<TelefonoSucursal> TelefonosSucursal { get; set; }
        public DbSet<Licencia> Licencias { get; set; }
        public DbSet<Encargado> Encargados { get; set; }
        public DbSet<TelefonoEncargado> TelefonosEncargado { get; set; }
        public DbSet<DireccionEncargado> DireccionesEncargado { get; set; }
        public DbSet<AccesoSucursalEncargado> AccesosSucursalEncargado { get; set; }
        public DbSet<UsuarioEmpresa> UsuariosEmpresa { get; set; }

        public DbContextPicturix(DbContextOptions<DbContextPicturix> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CategoriaMap());
            modelBuilder.ApplyConfiguration(new CategoriaPrecioMap());
            modelBuilder.ApplyConfiguration(new FotoMap());
            modelBuilder.ApplyConfiguration(new FotoPrecioMap());
            modelBuilder.ApplyConfiguration(new EmpresaMap());
            modelBuilder.ApplyConfiguration(new ModuloMap());
            modelBuilder.ApplyConfiguration(new AccesoPerfilMap());
            modelBuilder.ApplyConfiguration(new PerfilMap());
            modelBuilder.ApplyConfiguration(new PerfilEmpresaMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new DireccionMap());
            modelBuilder.ApplyConfiguration(new DireccionEmpresaMap());
            modelBuilder.ApplyConfiguration(new TipoTelefonoMap());
            modelBuilder.ApplyConfiguration(new TelefonoMap());
            modelBuilder.ApplyConfiguration(new TelefonoEmpresaMap());
            modelBuilder.ApplyConfiguration(new PersonaMap());
            modelBuilder.ApplyConfiguration(new AdministradorMap());
            modelBuilder.ApplyConfiguration(new TelefonoAdministradorMap());
            modelBuilder.ApplyConfiguration(new DireccionAdministradorMap());
            modelBuilder.ApplyConfiguration(new AccesoUsuarioMap());
            modelBuilder.ApplyConfiguration(new SucursalMap());
            modelBuilder.ApplyConfiguration(new TelefonoSucursalMap());
            modelBuilder.ApplyConfiguration(new LicenciaMap());
            modelBuilder.ApplyConfiguration(new EncargadoMap());
            modelBuilder.ApplyConfiguration(new TelefonoEncargadoMap());
            modelBuilder.ApplyConfiguration(new DireccionEncargadoMap());
            modelBuilder.ApplyConfiguration(new AccesoSucursalEncargadoMap());
            modelBuilder.ApplyConfiguration(new UsuarioEmpresaMap());

        }
    }
}
