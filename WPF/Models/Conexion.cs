﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Models
{
    public class Conexion
    {
        //public SqlConnection _Conexion = new SqlConnection(@"Data Source=den1.mssql8.gear.host;Initial Catalog=Supervix;User ID=supervix;Password=Wd7Lex7-S35~");
        public SqlConnection _Conexion = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Picturix;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        public DataTable Busquedas(SqlCommand SentenciaSQL)
        {
            DataTable Tabla = new DataTable();
            if (_Conexion != null && _Conexion.State == ConnectionState.Closed)
                _Conexion.Open();
            try
            {
                SentenciaSQL.Connection = _Conexion;
                SqlDataAdapter Adaptador = new SqlDataAdapter(SentenciaSQL);
                Adaptador.Fill(Tabla);
            }
            catch (Exception E)
            {
                string Error = E.Message;
                throw;
            }
            finally
            {
                _Conexion.Close();
            }
            return Tabla;
        }

        public DataTable Consultas(string Consulta)
        {
            DataTable Tabla = new DataTable();
            if (_Conexion != null && _Conexion.State == ConnectionState.Closed)
                _Conexion.Open();

            try
            {
                SqlCommand ComandoSQL = new SqlCommand(Consulta, _Conexion);
                SqlDataAdapter Adaptador = new SqlDataAdapter(ComandoSQL);
                Adaptador.Fill(Tabla);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                _Conexion.Close();
            }

            return Tabla;
        }

        public int EncontrarCoincidenciasSQL(String query)
        {
            int Coincidencias;
            DataTable dt = new DataTable();
            SqlCommand comando = new SqlCommand(query, _Conexion);
            SqlDataAdapter adap = new SqlDataAdapter(comando);
            adap.Fill(dt);
            DataRow row = dt.Rows[0];
            Coincidencias = Convert.ToInt32(row[0]);
            return Coincidencias;
        }

        public bool ExecuteTransaction(System.Collections.Generic.List<SqlCommand> LsCommands)
        {
            if (_Conexion != null && _Conexion.State == ConnectionState.Closed)
                _Conexion.Open();

            SqlTransaction sTransaction = this._Conexion.BeginTransaction(); ;
            try
            {

                foreach (SqlCommand cCommand in LsCommands)
                {
                    cCommand.Connection = this._Conexion;
                    cCommand.Transaction = sTransaction;
                    cCommand.ExecuteNonQuery();
                    cCommand.Dispose();
                }

                sTransaction.Commit();
                return true;
            }
            catch (Exception)
            {
                sTransaction.Rollback();
                throw;
            }
            finally
            {
                _Conexion.Close();
            }
        }

        public bool ManipulacionDeDatos(SqlCommand SentenciaSQL)
        {
            if (_Conexion != null && _Conexion.State == ConnectionState.Closed)
                _Conexion.Open();
            try
            {
                //Se le asigna la conexion
                SentenciaSQL.Connection = _Conexion;
                //Se ejecula el comando
                SentenciaSQL.ExecuteNonQuery();
                //Borrar el contenido de la consulta
                SentenciaSQL.Dispose();
                //Retorna verdaro
                return true;
            }
            catch (Exception)
            {
                //Retorna falso
                throw;
            }
            finally
            {
                _Conexion.Close();
            }
        }

    }
}
