﻿namespace WPF.Models.Picturix
{
    public class Sesion
    {
        public int IDUsuario { get; set; }
        public string Nombre { get; set; }
        public string ImagenPath { get; set; }

        public Sesion()
        {
            Nombre = "No disponible";
            ImagenPath = "Assets/usuario.jpg";
        }
    }
}
