﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace WPF.Models
{
    public class Licencia
    {
        public int IDLicencia { get; set; }
        public int IDSucursal { get; set; }
        public Guid Codigo { get; set; }
        public bool Estatus { get; set; }
        public bool EstatusOcupacion { get; set; }
        public string Sucursal { get; set; }
        public string Empresa { get; set; }

        public Conexion Con = new Conexion();

        public Licencia()
        {
            Codigo = Guid.Empty;
            Sucursal = "No disponible";
            Empresa = "No disponible";
        }

        public bool ActualizarEstatusOcupacionLicencia(Guid Codigo, bool Estatus)
        {
            SqlCommand ComandoSQL = new SqlCommand();
            try
            {
                ComandoSQL.CommandType = System.Data.CommandType.StoredProcedure;
                ComandoSQL.CommandText = "wpf_ActualizarEstatusOcupacionLicencia";

                ComandoSQL.Parameters.Add("@Codigo", SqlDbType.UniqueIdentifier);
                ComandoSQL.Parameters["@Codigo"].Value = Codigo;

                ComandoSQL.Parameters.Add("@EstatusOcupacion", SqlDbType.Bit);
                ComandoSQL.Parameters["@EstatusOcupacion"].Value = Estatus;

                Con.ManipulacionDeDatos(ComandoSQL);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public DataTable ObtenerLicencia(Guid Codigo)
        {
            SqlCommand ComandoSQL = new SqlCommand();
            DataTable Tabla = new DataTable();
            try
            {
                ComandoSQL.CommandType = System.Data.CommandType.StoredProcedure;
                ComandoSQL.CommandText = "wpf_ObtenerLicencia";

                ComandoSQL.Parameters.Add("@Codigo", SqlDbType.UniqueIdentifier);
                ComandoSQL.Parameters["@Codigo"].Value = Codigo;

                Tabla = Con.Busquedas(ComandoSQL);
            }
            catch (Exception)
            {
                throw;
            }
            return Tabla;
        }

    }
}
