﻿namespace WPF.Models
{
    public class TurnoIniciado
    {

        public int IDTurnoIniciado { get; set; }
        public int IDUsuario { get; set; }
        public int IDSucursal { get; set; }
        public int IDCierre { get; set; }
        public string Folio { get; set; }
        public string FechaApertura { get; set; }
        public string HoraApertura { get; set; }
        public int? IDSucursalCierre { get; set; }
        public string FechaCierre { get; set; }
        public string HoraCierre { get; set; }
        public string Estatus { get; set; }

        public string Encargado { get; set; }
        public string ImagenPath { get; set; }

        public TurnoIniciado()
        {
            Folio = "No disponible";
            FechaApertura = "No disponible";
            HoraApertura = "No disponible";
            Encargado = "No disponible";
            ImagenPath = "Assets/usuario.jpg";



        }

    }
}
