﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPF.ViewModels;

namespace WPF
{
    /// <summary>
    /// Lógica de interacción para Picturix.xaml
    /// </summary>
    public partial class Picturix : Window
    {
        public Picturix()
        {
            InitializeComponent();
            this.DataContext = new VMPicturix();
        }
       
    }
}
