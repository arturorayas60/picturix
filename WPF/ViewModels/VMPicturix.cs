﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using WPF.Models;
using WPF.Models.Picturix;

namespace WPF.ViewModels
{
    public class VMPicturix : INotifyPropertyChanged
    {

        #region Generic PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        //#region Singleton
        //public static VMRecoleccion Instance { get; set; }
        //public static VMRecoleccion GetInstance()
        //{
        //    if (Instance == null)
        //        Instance = new VMRecoleccion();

        //    return Instance;
        //}
        //#endregion

        #endregion


        #region Atributos

        private string _version = string.Empty;
        public string Version
        {
            get { return _version; }
            set
            {
                _version = value;
                OnPropertyChanged("Version");
            }
        }

        private string _txtLicencia = string.Empty;
        public string txtLicencia
        {
            get { return _txtLicencia; }
            set
            {
                _txtLicencia = value;
                OnPropertyChanged("txtLicencia");
            }
        }

        private Licencia _Licencia = new Licencia();
        public Licencia LicenciaActual
        {
            get { return _Licencia; }
            set
            {
                _Licencia = value;
                OnPropertyChanged("LicenciaActual");
            }
        }

        private Sesion _SesionActual = new Sesion();
        public Sesion SesionActual
        {
            get { return _SesionActual; }
            set
            {
                _SesionActual = value;
                OnPropertyChanged("SesionActual");
            }
        }

        private TurnoIniciado _TurnoIniciado = new TurnoIniciado();
        public TurnoIniciado TurnoIniciadoActual
        {
            get { return _TurnoIniciado; }
            set
            {
                _TurnoIniciado = value;
                OnPropertyChanged("TurnoIniciadoActual");
            }
        }

        private bool _IsDisplayMenu = true;
        public bool IsDisplayMenu
        {
            get { return _IsDisplayMenu; }
            set
            {
                _IsDisplayMenu = value;
                OnPropertyChanged("IsDisplayMenu");
            }
        }

        private bool _IsEnabledIniciarTurno = true;
        public bool IsEnabledIniciarTurno
        {
            get { return _IsEnabledIniciarTurno; }
            set
            {
                _IsEnabledIniciarTurno = value;
                OnPropertyChanged("IsEnabledIniciarTurno");
            }
        }

        private bool _IsEnabledCerrarTurno = true;
        public bool IsEnabledCerrarTurno
        {
            get { return _IsEnabledCerrarTurno; }
            set
            {
                _IsEnabledCerrarTurno = value;
                OnPropertyChanged("IsEnabledCerrarTurno");
            }
        }

        private bool _IsEnabledIniciarSesion = true;
        public bool IsEnabledIniciarSesion
        {
            get { return _IsEnabledIniciarSesion; }
            set
            {
                _IsEnabledIniciarSesion = value;
                OnPropertyChanged("IsEnabledIniciarSesion");
            }
        }

        private bool _IsEnabledCerrarSesion = true;
        public bool IsEnabledCerrarSesion
        {
            get { return _IsEnabledCerrarSesion; }
            set
            {
                _IsEnabledCerrarSesion = value;
                OnPropertyChanged("IsEnabledCerrarSesion");
            }
        }

        private bool _IsEnabledIngresarLicencia = true;
        public bool IsEnabledIngresarLicencia
        {
            get { return _IsEnabledIngresarLicencia; }
            set
            {
                _IsEnabledIngresarLicencia = value;
                OnPropertyChanged("IsEnabledIngresarLicencia");
            }
        }

        private bool _IsEnabledQuitarLicencia = true;
        public bool IsEnabledQuitarLicencia
        {
            get { return _IsEnabledQuitarLicencia; }
            set
            {
                _IsEnabledQuitarLicencia = value;
                OnPropertyChanged("IsEnabledQuitarLicencia");
            }
        }

        private bool _IsOpenIngresarLicencia = false;
        public bool IsOpenIngresarLicencia
        {
            get { return _IsOpenIngresarLicencia; }
            set
            {
                _IsOpenIngresarLicencia = value;
                OnPropertyChanged("IsOpenIngresarLicencia");
            }
        }

        private bool _IsOpenIniciarTurno = false;
        public bool IsOpenIniciarTurno
        {
            get { return _IsOpenIniciarTurno; }
            set
            {
                _IsOpenIniciarTurno = value;
                OnPropertyChanged("IsOpenIniciarTurno");
            }
        }

        #endregion

        #region ICommands

        private ICommand commandIniciarTurno;
        public ICommand CommandIniciarTurno
        {
            get
            {
                if (commandIniciarTurno == null)
                    commandIniciarTurno = new IniciarTurno_Click();
                return commandIniciarTurno;
            }
            set
            {
                commandIniciarTurno = value;
            }
        }
        class IniciarTurno_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                var vm = parameter as VMPicturix;
                vm.IsOpenIniciarTurno = true;
            }
            #endregion
        }

        private ICommand commandIniciarNuevoTurno;
        public ICommand CommandIniciarNuevoTurno
        {
            get
            {
                if (commandIniciarNuevoTurno == null)
                    commandIniciarNuevoTurno = new IniciarNuevoTurno_Click();
                return commandIniciarNuevoTurno;
            }
            set
            {
                commandIniciarNuevoTurno = value;
            }
        }
        class IniciarNuevoTurno_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                var vm = parameter as VMPicturix;
                vm.IsOpenIniciarTurno = true;
            }
            #endregion
        }

        private ICommand commandCerrarTurno;
        public ICommand CommandCerrarTurno
        {
            get
            {
                if (commandCerrarTurno == null)
                    commandCerrarTurno = new CerrarTurno_Click();
                return commandCerrarTurno;
            }
            set
            {
                commandCerrarTurno = value;
            }
        }
        class CerrarTurno_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                Picturix window = Application.Current.Windows.OfType<Picturix>().SingleOrDefault(x => x.IsActive);
                window.WindowState = WindowState.Minimized;
            }
            #endregion
        }

        private ICommand commandIngresarLicencia;
        public ICommand CommandIngresarLicencia
        {
            get
            {
                if (commandIngresarLicencia == null)
                    commandIngresarLicencia = new IngresarLicencia_Click();
                return commandIngresarLicencia;
            }
            set
            {
                commandIngresarLicencia = value;
            }
        }
        class IngresarLicencia_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                var vm = parameter as VMPicturix;
                vm.IsOpenIngresarLicencia = true;
            }
            #endregion
        }

        private ICommand commandEstablecerLicencia;
        public ICommand CommandEstablecerLicencia
        {
            get
            {
                if (commandEstablecerLicencia == null)
                    commandEstablecerLicencia = new EstablecerLicencia_Click();
                return commandEstablecerLicencia;
            }
            set
            {
                commandEstablecerLicencia = value;
            }
        }
        class EstablecerLicencia_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                var vm = parameter as VMPicturix;

                try
                {
                    if (!string.IsNullOrEmpty(vm.txtLicencia))
                    {
                        Guid UIDLicencia = new Guid(vm.txtLicencia);
                        if (vm.ValidarLicencia(UIDLicencia))
                        {
                            if (vm.GuardarConfiguracionLicencia(UIDLicencia))
                            {
                                if (vm.ActualizarEstatusOcupacionLicencia(UIDLicencia, true))
                                {
                                    vm.ObtenerLicenciaSucursal(UIDLicencia);
                                    vm.RecuperarTurnoSinCerrar();
                                    vm.CargarControlesDefault();
                                    vm._IsOpenIngresarLicencia = false;
                                    MessageBoxResult Mensaje = MessageBox.Show("Se ha activado la licencia en este equipo", "Licencia Aceptada", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                        }
                    }
                    else { MessageBox.Show("Ingrese una licencia", "Campo licencia vacio", MessageBoxButton.OK, MessageBoxImage.Exclamation); }
                }
                catch (Exception)
                {
                    MessageBox.Show("Ingrese una licencia valida", "Licencia invalida", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
            #endregion
        }

        private ICommand commandQuitarLicencia;
        public ICommand CommandQuitarLicencia
        {
            get
            {
                if (commandQuitarLicencia == null)
                    commandQuitarLicencia = new QuitarLicencia_Click();
                return commandQuitarLicencia;
            }
            set
            {
                commandQuitarLicencia = value;
            }
        }
        class QuitarLicencia_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                Picturix window = Application.Current.Windows.OfType<Picturix>().SingleOrDefault(x => x.IsActive);
                window.WindowState = WindowState.Minimized;
            }
            #endregion
        }

        private ICommand commandMinimizar;
        public ICommand CommandMinimizar
        {
            get
            {
                if (commandMinimizar == null)
                    commandMinimizar = new Minimizar_Click();
                return commandMinimizar;
            }
            set
            {
                commandMinimizar = value;
            }
        }
        class Minimizar_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                Picturix window = Application.Current.Windows.OfType<Picturix>().SingleOrDefault(x => x.IsActive);
                window.WindowState = WindowState.Minimized;
            }
            #endregion
        }

        private ICommand commandCerrar;
        public ICommand CommandCerrar
        {
            get
            {
                if (commandCerrar == null)
                    commandCerrar = new Cerrar_Click();
                return commandCerrar;
            }
            set
            {
                commandCerrar = value;
            }
        }
        class Cerrar_Click : ICommand
        {
            #region ICommand Members  
            public bool CanExecute(object parameter)
            {
                return true;
            }
            public event EventHandler CanExecuteChanged
            {
                add { CommandManager.RequerySuggested += value; }
                remove { CommandManager.RequerySuggested -= value; }
            }
            public void Execute(object parameter)
            {
                Picturix window = Application.Current.Windows.OfType<Picturix>().SingleOrDefault(x => x.IsActive);
                window.Close();
            }
            #endregion
        }

        #endregion

        public VMPicturix()
        {
            if (VerificarLicenciaExistenteWPF())
            {
                var licencia = Registry.CurrentUser.OpenSubKey("Picturix", true).GetValue("Licencia");
                ObtenerLicenciaSucursal(new Guid(licencia.ToString()));
                RecuperarTurnoSinCerrar();

                Assembly assembly = Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                Version = fvi.FileVersion;
            }
            CargarControlesDefault();
        }

        #region Metodos

        public bool ActualizarEstatusOcupacionLicencia(Guid Codigo, bool Estatus)
        {
            try
            {
                return new Licencia().ActualizarEstatusOcupacionLicencia(Codigo, Estatus);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void CargarControlesDefault()
        {
            try
            {
                if (LicenciaActual.IDLicencia > 0)
                {
                    ControladorVista("LicenciaActiva");
                }
                else
                {
                    ControladorVista("");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ControladorVista(string caso)
        {
            switch (caso)
            {
                case "LicenciaActiva":
                    IsEnabledIngresarLicencia = false;
                    IsEnabledQuitarLicencia = true;

                    IsEnabledIniciarTurno = true;
                    IsEnabledCerrarTurno = false;

                    IsEnabledIniciarSesion = true;
                    IsEnabledCerrarSesion = false;
                    break;
                case "LicenciaInactiva":

                    break;
                case "TurnoIniciado":

                    break;
                default:
                    IsEnabledIngresarLicencia = true;
                    IsEnabledQuitarLicencia = false;

                    IsEnabledIniciarTurno = false;
                    IsEnabledCerrarTurno = false;

                    IsEnabledIniciarSesion = false;
                    IsEnabledCerrarSesion = false;
                    break;
            }
        }

        public bool GuardarConfiguracionLicencia(Guid UIDLicencia)
        {
            try
            {
                var registro = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Picturix", true);
                if (registro == null)
                {
                    Registry.CurrentUser.CreateSubKey("Picturix");
                }
                RegistryKey key = Registry.CurrentUser.OpenSubKey("Picturix", true);
                key.SetValue("Licencia", UIDLicencia.ToString());
                key.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public void IniciarTurnoIniciado()
        {


            //string Usuario = txtUsuario.Text;
            //string Contraseña = txtContraseña.Password.ToString();
            //Guid Turno;
            //if (cbTurnos.SelectedItem != null)
            //    Turno = Guid.Parse(cbTurnos.SelectedValue.ToString());
            //else
            //    Turno = Guid.Empty;

            //if (_VmIniciarTurno.IniciarTurno(Usuario, Contraseña, Turno, _UIDLicencia, _UIDSucursal, _TituloPantalla))
            //{
            //    if (_VmIniciarTurno.GuardarTurnoIniciado(_VmIniciarTurno._NuevoTurnoIniciado))
            //    {
            //        if (_VmIniciarTurno.GuardarCierre(_VmIniciarTurno._NuevoTurnoIniciado.FOLIO, _VmIniciarTurno._NuevoTurnoIniciado.UIDSUCURSALREF, _VmIniciarTurno._NuevoTurnoIniciado.uIDTurnoIniciado))
            //        {
            //            _WSupervix.txtUIDTurno.Text = _VmIniciarTurno._NuevoTurnoIniciado.uIDTurnoIniciado.ToString();
            //            _WSupervix.txtUIDUsuario.Text = _VmIniciarTurno._NuevoTurnoIniciado.UIDUsuario.ToString();
            //            _WSupervix.CargarControlesDefault();
            //            this.DialogResult = false;

            //            Dispatcher.Invoke(() =>
            //            {
            //                progreso.Close();
            //                MessageBox.Show("El turno fue iniciado correctamente " + _VmIniciarTurno._LocalTime.Value.ToString("yyyy/MM/dd HH:mm:ss"), "Registro exitoso", MessageBoxButton.OK, MessageBoxImage.Information);
            //            });

            //        }
            //    }
            //}
            //else
            //{
            //    btnAceptar.IsEnabled = true;
            //    this.IsEnabled = true;
            //    MessageBox.Show(_VmIniciarTurno._MESSAGE, _VmIniciarTurno._CAPTIONERROR, MessageBoxButton.OK, MessageBoxImage.Stop);
            //}

        }

        public void ObtenerLicenciaSucursal(Guid codigo)
        {
            try
            {
                foreach (DataRow item in new Licencia().ObtenerLicencia(codigo).Rows)
                {
                    Licencia licencia = new Licencia()
                    {
                        IDLicencia = int.Parse(item["IDLicencia"].ToString()),
                        IDSucursal = int.Parse(item["IDSucursal"].ToString()),
                        Codigo = codigo,
                        Estatus = bool.Parse(item["Estatus"].ToString()),
                        EstatusOcupacion = bool.Parse(item["Ocupacion"].ToString()),
                        Sucursal = item["Sucursal"].ToString(),
                        Empresa = item["Empresa"].ToString()
                    };
                    LicenciaActual = licencia;
                };

            }
            catch (Exception)
            {

            }
        }

        public void RecuperarTurnoSinCerrar()
        {

        }

        public bool ValidarLicencia(Guid codigo)
        {
            ObtenerLicenciaSucursal(codigo);
            if (LicenciaActual.IDLicencia > 0)
            {
                if (LicenciaActual.Estatus)
                {
                    if (!LicenciaActual.EstatusOcupacion) { return true; }
                    else { MessageBox.Show("Ingrese una licencia valida", "Licencia ocupada", MessageBoxButton.OK, MessageBoxImage.Exclamation); }
                    txtLicencia = string.Empty;
                    return false;
                }
                else { MessageBox.Show("Ingrese una licencia valida", "Licencia inactiva", MessageBoxButton.OK, MessageBoxImage.Exclamation); txtLicencia = string.Empty; return false; }
            }
            else { MessageBox.Show("Ingrese una licencia valida", "Licencia invalida", MessageBoxButton.OK, MessageBoxImage.Exclamation); txtLicencia = string.Empty; return false; }
        }

        public bool VerificarLicenciaExistenteWPF()
        {
            var sourceRegistro = Registry.GetValue(@"HKEY_CURRENT_USER\Picturix", "Licencia", null);

            if (sourceRegistro != null)
                return true;

            return false;
        }

        #endregion



    }
}
