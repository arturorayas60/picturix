﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Personas
{
    public class Persona
    {
        [Key]
        public int IDPersona { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "El nombre no debe tener más de 50 caracteres")]
        public string Nombre { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "El apellido paterno no debe tener más de 50 caracteres")]
        public string ApellidoPaterno { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "El apellido materno no debe tener más de 50 caracteres")]
        public string ApellidoMaterno { get; set; }
 
        public string Sexo { get; set; }
  
        public DateTime? FechaNacimiento { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(50, ErrorMessage = "El email no debe tener más de 50 caracteres")]
        public string Email { get; set; }


    }
}
