﻿using Entities.Administradores;
using Entities.Telefonos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Encargados
{
    public class TelefonoEncargado
    {
        [Key]
        public int IDTelefonoEncargado { get; set; }

        public int IDEncargado { get; set; }

        public int IDTelefono { get; set; }


        [ForeignKey("IDEncargado")]
        public Encargado encargado { get; set; }

        [ForeignKey("IDTelefono")]
        public Telefono telefono { get; set; }
    }
}
