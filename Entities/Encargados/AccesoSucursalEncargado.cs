﻿using Entities.Sucursales;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Encargados
{
    public class AccesoSucursalEncargado
    {
        [Key]
        public int IDAccesoSucursalEncargado { get; set; }
        public int IDSucursal { get; set; }
        public int IDEncargado { get; set; }
        [Required]
        public bool Estatus { get; set; }

        [ForeignKey("IDSucursal")]
        public Sucursal sucursal { get; set; }

        [ForeignKey("IDEncargado")]
        public Encargado encargado { get; set; }
    }
}
