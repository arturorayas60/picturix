﻿using Entities.Direcciones;
using Entities.Empresas;
using Entities.Personas;
using Entities.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Encargados
{
    public class DireccionEncargado
    {
        [Key]
        public int IDDireccionEncargado { get; set; }

        public int IDEncargado { get; set; }

        public int IDDireccion { get; set; }


        [ForeignKey("IDEncargado")]
        public Encargado encargado { get; set; }

        [ForeignKey("IDDireccion")]
        public Direccion direccion { get; set; }

    }
}
