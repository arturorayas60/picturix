﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Entities.Empresas;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Direcciones
{
    public class Direccion
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int IDDireccion { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "El identificador no debe tener más de 13 caracteres")]
        public string Identificador { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "El pais no debe tener más de 80 caracteres")]
        public string Pais { get; set; }
        
        [Required]
        public int IDEstado { get; set; }
        
        [Required]
        [StringLength(80, ErrorMessage = "El estado no debe tener más de 80 caracteres")]
        public string Estado { get; set; }

        [Required]
        public int IDMunicipio { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "El municipio no debe tener más de 80 caracteres")]
        public string Municipio { get; set; }

        [Required]
        public int IDLocalidad { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "La localidad no debe tener más de 80 caracteres")]
        public string Localidad { get; set; }

        [Required]
        public int IDColonia { get; set; }

        [Required]
        [StringLength(80, ErrorMessage = "La colonia no debe tener más de 80 caracteres")]
        public string Colonia { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "La calle no debe tener más de 50 caracteres")]
        public string Calle { get; set; }

        [StringLength(10, ErrorMessage = "El numero interior no debe tener más de 10 caracteres")]
        public string NoInterior { get; set; }

        [StringLength(10, ErrorMessage = "El numero exterior no debe tener más de 10 caracteres")]
        public string NoExterior { get; set; }

        [StringLength(60, ErrorMessage = "El entre calle no debe tener más de 60 caracteres")]
        public string EntreCalles { get; set; }

        [StringLength(90, ErrorMessage = "La referencia no debe tener más de 90 caracteres")]
        public string Referencias { get; set; }

        //public Empresa empresa { get; set; }

        //public ICollection<DireccionEmpresa> Direcciones { get; set; }
    }
}
