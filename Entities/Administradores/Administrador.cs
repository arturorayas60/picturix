﻿using Entities.Empresas;
using Entities.Personas;
using Entities.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Administradores
{
    public class Administrador
    {
        [Key]
        public int IDAdministrador { get; set; }
        public int IDPersona { get; set; }
        public int IDUsuario { get; set; }
        public int IDEmpresa { get; set; }
        [Required]
        public bool Estatus { get; set; }

        [ForeignKey("IDPersona")]
        public Persona persona { get; set; }
        [ForeignKey("IDUsuario")]
        public Usuario usuario { get; set; }
        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }



    }
}
