﻿using Entities.Direcciones;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Administradores
{
    public class DireccionAdministrador
    {
        [Key]
        public int IDDireccionAdministrador { get; set; }

        public int IDAdministrador { get; set; }

        public int IDDireccion { get; set; }

        [ForeignKey("IDDireccion")]
        public Direccion direccion { get; set; }

        [ForeignKey("IDAdministrador")]
        public Administrador administrador { get; set; }
    }
}
