﻿using Entities.Empresas;
using Entities.Telefonos;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Administradores
{
    public class TelefonoAdministrador
    {
        [Key]
        public int IDTelefonoAdministrador { get; set; }

        public int IDAdministrador { get; set; }

        public int IDTelefono { get; set; }


        [ForeignKey("IDAdministrador")]
        public Administrador administrador { get; set; }

        [ForeignKey("IDTelefono")]
        public Telefono telefono { get; set; }
    }
}
