﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Telefonos
{
    public class Telefono
    {       
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int IDTelefono { get; set; }
        public int IDTipoTelefono { get; set; }
        [Required]
        [StringLength(5, ErrorMessage = "La lada no debe tener más de 5 caracteres")]
        public string Lada { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "La numero de telefono no debe tener más de 10 caracteres")]
        public string NoTelefono { get; set; }
        [ForeignKey("IDTipoTelefono")]
        public TipoTelefono tipotelefono { get; set; }
    }
}
