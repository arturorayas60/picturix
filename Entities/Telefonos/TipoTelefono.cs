﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Telefonos
{
    public class TipoTelefono
    {
        [Key]
        public int IDTipoTelefono { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "La lada no debe tener más de 30 caracteres")]
        public string Nombre { get; set; }
        //public ICollection<Telefono> telefonos { get; set; }
    }
}
