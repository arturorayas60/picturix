﻿using Entities.Empresas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Categorias
{
    public class CategoriaPrecio
    {
        [Key]
        public int IDCategoriaPrecio { get; set; }

        public int IDCategoria { get; set; }

        public int IDEmpresa { get; set; }

        public double Precio { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }
    }
}
