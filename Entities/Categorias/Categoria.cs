﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Categorias
{
    public class Categoria
    {
        [Key]
        public int IdCategoria { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "El nombre no debe tener más de 50 caracteres")]
        public string Nombre { get; set; }

        [Required]
        public string IdGoogleDrive { get; set; }

        public int Padre { get; set; }

        public double Precio { get; set; }

        public bool Estatus {get;set;}

    }
}
