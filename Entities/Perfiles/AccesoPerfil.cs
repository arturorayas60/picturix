﻿using Entities.Modulos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Perfiles
{
    public class AccesoPerfil
    {
        [Key]
        public int IDAccesoPerfil { get; set; }
        
        public int IDPerfil { get; set; }

        public int IDModulo { get; set; }

        public bool Estatus { get; set; }

        [ForeignKey("IDPerfil")]
        public Perfil perfil { get; set; }

        [ForeignKey("IDModulo")]
        public Modulo modulo { get; set; }


    }
}
