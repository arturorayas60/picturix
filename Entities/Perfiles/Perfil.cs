﻿using Entities.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Perfiles
{
    public class Perfil
    {
        [Key]
        public int IDPerfil { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "El nombre no debe tener más de 25 caracteres")]
        public string Nombre { get; set; }

        [Required]
        public int Nivel { get; set; }
        
        public ICollection<Usuario> usuarios { get; set; }

    }
}
