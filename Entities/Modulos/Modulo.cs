﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Modulos
{
    public class Modulo
    {
        [Key]
        public int IDModulo { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "El nombre no debe tener más de 30 caracteres")]
        public string Nombre { get; set; }

        [Required]
        public int Nivel { get; set; }

    }
}
