﻿using Entities.Categorias;
using Entities.Empresas;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Fotos
{
    public class Foto
    {
        [Key]
        public int IDFoto { get; set; }

        public int IDCategoria { get; set; }

        public string Nombre { get; set; }

        public string IDGoogleDrive { get; set; }

        public double Precio { get; set; }

        public string UrlGoogleDrive { get; set; }

        [ForeignKey("IDCategoria")]
        public Categoria categoria { get; set; }
    }
}
