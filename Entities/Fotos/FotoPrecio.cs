﻿using Entities.Empresas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Fotos
{
    public class FotoPrecio
    {
        [Key]
        public int IDFotoPrecio { get; set; }

        public int IDFoto { get; set; }

        public int IDEmpresa { get; set; }

        public double Precio { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }
    }
}
