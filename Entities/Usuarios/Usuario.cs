﻿using Entities.Perfiles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Usuarios
{
   public class Usuario
    {
		[Key]
		[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
		public int IDUsuario { get; set; }

		public int IDPerfil { get; set; }

		[Required]
		[StringLength(90, ErrorMessage = "El nombre de usuario no debe tener más de 90 caracteres")]
		public string Nombre { get; set; }

		[Required]
		public byte[] Password_hash { get; set; }

		[Required]
		public byte[] Password_salt { get; set; }

		public bool Estatus { get; set; }

		[ForeignKey("IDPerfil")]
		public Perfil perfil { get; set; }
    }
}
