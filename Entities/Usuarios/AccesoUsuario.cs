﻿using Entities.Modulos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Usuarios
{
    public class AccesoUsuario
    {
        [Key]
        public int IDAccesoUsuario { get; set; }

        [Required]
        public int IDUsuario { get; set; }

        [Required]
        public int IDModulo { get; set; }

        [Required]
        public bool Estatus { get; set; }


        [ForeignKey("IDUsuario")]
        public List<Usuario> usuario { get; set; }

        [ForeignKey("IDModulo")]
        public List<Modulo> modulos { get; set; }


    }
}
