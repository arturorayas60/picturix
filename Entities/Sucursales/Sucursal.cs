﻿using Entities.Direcciones;
using Entities.Empresas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Sucursales
{
    public class Sucursal
    {
        [Key]
        public int IDSucursal { get; set; }

        public int IDEmpresa { get; set; }

        public int IDDireccion { get; set; }

        [Required]
        [StringLength(90, ErrorMessage = "El nombre no debe tener más de 90 caracteres")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "El identificador no debe tener más de 30 caracteres")]
        public string Identificador { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "La hora de apertura no debe tener más de 5 caracteres")]
        public string HoraApertura { get; set; }

        [Required]
        [StringLength(5, ErrorMessage = "El hora de cierre no debe tener más de 5 caracteres")]
        public string HoraCierre { get; set; }

        [Required]
        public bool Estatus { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "El tipo no debe tener más de 20 caracteres")]
        public string Tipo { get; set; }

        [Required]
        [StringLength(90, ErrorMessage = "La zona horaria no debe tener más de 90 caracteres")]
        public string ZonaHoraria { get; set; }


        [ForeignKey("IDDireccion")]
        public Direccion direccion { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }

    }
}
