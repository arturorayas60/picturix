﻿using Entities.Telefonos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Sucursales
{
    public class TelefonoSucursal
    {
        [Key]
        public int IDTelefonoSucursal { get; set; }

        public int IDSucursal { get; set; }

        public int IDTelefono { get; set; }

        [ForeignKey("IDTelefono")]
        public Telefono telefono { get; set; }

        [ForeignKey("IDSucursal")]
        public Sucursal sucursal { get; set; }
    }
}
