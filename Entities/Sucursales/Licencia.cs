﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Sucursales
{
    public class Licencia
    {
        [Key]
        public int IDLicencia { get; set; }

        [Required]
        public int IDSucursal { get; set; }

        [Required]
        public Guid Codigo { get; set; }

        [Required]
        public bool Estatus { get; set; }

        [Required]
        public bool Ocupacion { get; set; }

        [ForeignKey("IDSucursal")]
        public Sucursal sucursal { get; set; }

    }
}
