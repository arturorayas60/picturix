﻿using Entities.Direcciones;
using Entities.Telefonos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Empresas
{
    public class Empresa
    {
        [Key]
        public int IDEmpresa { get; set; }
        [Required]
        public string RFC { get; set; }
        [Required]
        public string NombreComercial { get; set; }
        [Required]
        public string RazonSocial { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public bool? Estatus { get; set; }
        public DateTime? FechaCreacion { get; set; }


    }
}
