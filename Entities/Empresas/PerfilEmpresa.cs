﻿using Entities.Perfiles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Empresas
{
    public class PerfilEmpresa
    {
        [Key]
        public int IDPerfilEmpresa { get; set; }

        public int IDEmpresa { get; set; }

        public int IDPerfil { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }

        [ForeignKey("IDPerfil")]
        public Perfil perfil { get; set; }


    }
}
