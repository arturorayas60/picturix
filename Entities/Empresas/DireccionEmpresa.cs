﻿using Entities.Direcciones;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Empresas
{
    public class DireccionEmpresa
    {
        [Key]
        public int IDDireccionEmpresa { get; set; }

        public int IDEmpresa { get; set; }

        public int IDDireccion { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }

    }
}
