﻿using Entities.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Empresas
{
   public class UsuarioEmpresa
    {
        [Key]
        public int IDUsuarioEmpresa { get; set; }

        public int IDEmpresa { get; set; }

        public int IDUsuario { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }

        [ForeignKey("IDUsuario")]
        public Usuario usuario { get; set; }
    }
}
