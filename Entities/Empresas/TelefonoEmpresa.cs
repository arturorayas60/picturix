﻿using Entities.Telefonos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Empresas
{
    public class TelefonoEmpresa
    {
        [Key]
        public int IDTelefonoEmpresa { get; set; }

        public int IDEmpresa { get; set; }

        public int IDTelefono { get; set; }

        [ForeignKey("IDEmpresa")]
        public Empresa empresa { get; set; }

    }
}
